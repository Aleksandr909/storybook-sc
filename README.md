# Shorelin

[![pipeline status](https://gitlab.com/Aleksandr909/storybook-sc/badges/master/pipeline.svg)](https://gitlab.com/Aleksandr909/storybook-sc/-/commits/master)

### [Приложение](https://storybook.vorfolio.ru/)

#### О приложении

В текущем проекте собраны компоненты в основном из открытого доступа (на html + css + js, ссылка на источник добавлена в документации скопированных комнпонент), написанные/переписанные на react + styled-components. В большинстве компонент добавлена **темизация**.

#### Темизация

Для работы с темизированными компонентами в проекте нужно скопировать Provider с [темой]() на нужный уровень в приложении

```tsx
    <ThemeProvider theme={{ ...themeObj, style: darkMode ? 'dark' : 'light' }}>
        {children}
    <ThemeProvider />
```

#### О сервере

Проект размещен на сервере Vscale с Ubuntu OS. Настроено https (с помощью **letsencrypt** создан ssl сертификат). С помощью **[gitlab ci](https://gitlab.com/Aleksandr909/storybook-sc/-/blob/master/.gitlab-ci.yml)** проект автоматически собирается на серверах gitlab и затем переносится на сервер. На сервере с помощью **nginx** приложение проксируется с порта на нужный поддомен.

### Локальное тестирование

```sh
git clone https://gitlab.com/Aleksandr909/storybook-sc.git
cd storybook-sc
npm install
npm run storybook
```

### Лицензия

[BSD 2-clause "Simplified" License](https://gitlab.com/Aleksandr909/storybook-sc/-/blob/master/LICENSE)
