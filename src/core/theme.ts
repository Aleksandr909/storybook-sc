const themeObj = {
  colors: {
    body: { light: '#ffffff', dark: '#263238' },
    antiBody: { light: '#263238', dark: '#ffffff' },
    main: { light: 'blue', dark: 'white' },
    primary: { light: '#039BE5', dark: '#81D4FA' },
    secondary: { light: '#D32F2F', dark: '#EF9A9A' },
    default: { light: '#333', dark: '#eee' },
    primaryTransparent: { light: 'rgba(3, 155, 229, 0.3)', dark: 'rgba(129, 212, 250, 0.3)' },
    textPrimary: { light: '#000000', dark: '#ffffff' },
    textPrimaryTransparent: { light: 'rgba(0, 0, 0 , 0.5)', dark: 'rgba(255, 255, 255, 0.5)' },
    antiTextPrimary: { light: '#ffffff', dark: '#000000' },
    linkLine: { light: 'linear-gradient(90deg, #00B8D4, #00E5FF)', dark: 'linear-gradient(90deg, #84FFFF, #18FFFF)' },
  },
}
export default themeObj
