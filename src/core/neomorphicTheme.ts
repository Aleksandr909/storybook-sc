const neomorphicTheme = {
  colors: {
    body: { light: '#ECEFF1', dark: '#263238' },
    antiBody: { light: '#263238', dark: '#ECEFF1' },
    primary: { light: '#FF6D00', dark: '#FFD180' },
    default: { light: '#000000', dark: '#ECEFF1' },
    bg: {
      outer: 'linear-gradient(135deg, rgba(0, 0, 0, 0.05) 0%, rgba(255, 255, 255, 0.05) 100%)',
      inner: 'linear-gradient(135deg, rgba(255, 255, 255, 0.05) 0%, rgba(0, 0, 0, 0.05) 100%)',
      full: 'linear-gradient(135deg, rgba(255, 255, 255, 0.05) 0%, rgba(0, 0, 0, 0.05) 100%)',
    },
    textColor1: { light: '#263238', dark: '#ECEFF1' },
    textColor3: { light: '#546E7A', dark: '#B0BEC5' },
    textColor5: { light: '#78909C', dark: '#78909C' },
    textColor7: { light: '#B0BEC5', dark: '#546E7A' },
    textColor9: { light: '#ECEFF1', dark: '#37474F' },
  },
  shadows: {
    outer: {
      dark:
        '-2px -2px 4px rgba(255, 255, 255, 0.07), 3px 3px 6px rgba(0, 0, 0, 0.7), inset -2px -2px 4px rgba(0, 0, 0, 0.8), inset 2px 2px 4px rgba(255, 255, 255, 0.13)',
      light:
        '3px 3px 6px rgba(0, 0, 0, 0.1), -2px -2px 4px rgba(255, 255, 255, 0.8), inset 2px 2px 4px rgba(255, 255, 255, 0.5), inset -2px -2px 4px rgba(0, 0, 0, 0.1)',
    },
    outerHover: {
      dark:
        '-2px -2px 4px rgba(255, 255, 255, 0.07), 3px 3px 6px rgba(0, 0, 0, 0.8), inset -2px -2px 4px #000000, inset 2px 2px 4px rgba(255, 255, 255, 0.2)',
      light:
        '3px 3px 6px rgba(0, 0, 0, 0.2), -2px -2px 4px rgba(255, 255, 255, 0.8), inset 2px 2px 4px rgba(255, 255, 255, 0.7), inset -2px -2px 4px rgba(0, 0, 0, 0.1)',
    },
    outerOnly: {
      dark:
        '-1px -1px 2px rgba(255, 255, 255, 0.1), 1px 1px 2px rgba(0, 0, 0, 0.7)',
      light:
        '1px 1px 2px rgba(0, 0, 0, 0.1), -1px -1px 2px rgba(255, 255, 255, 0.7)',
    },
    outerOnlyHover: {
      dark:
        '-1px -1px 2px rgba(255, 255, 255, 0.15), 1px 1px 2px rgba(0, 0, 0, 0.8)',
      light:
        '1px 1px 2px rgba(0, 0, 0, 0.15), -1px -1px 2px rgba(255, 255, 255, 0.8)',
    },
    inner: {
      dark: 'inset -2px -2px 4px rgba(255, 255, 255, 0.13), inset 2px 2px 4px rgba(0, 0, 0, 0.8)',
      light: 'inset 2px 2px 4px rgba(0, 0, 0, 0.05), inset -2px -2px 4px rgba(255, 255, 255, 0.5)',
    },
    innerHover: {
      dark: 'inset -3px -3px 6px rgba(255, 255, 255, 0.28), inset 3px 3px 6px rgba(0, 0, 0, 0.85)',
      light: 'inset 3px 3px 6px rgba(0, 0, 0, 0.1), inset -3px -3px 6px rgba(255, 255, 255, 0.7)',
    },
    full: {
      dark:
        '-2px -2px 4px rgba(255, 255, 255, 0.07), 3px 3px 6px rgba(0, 0, 0, 0.7), inset -2px -2px 4px rgba(255, 255, 255, 0.13), inset 2px 2px 4px rgba(0, 0, 0, 0.8)',
      light:
        '3px 3px 6px rgba(0, 0, 0, 0.1), -2px -2px 4px rgba(255, 255, 255, 0.8), inset 2px 2px 4px rgba(0, 0, 0, 0.05), inset -2px -2px 4px rgba(255, 255, 255, 0.5)',
    },
    fullHover: {
      dark:
        '-2px -2px 4px rgba(255, 255, 255, 0.07), 3px 3px 6px rgba(0, 0, 0, 0.8), inset -3px -3px 6px rgba(255, 255, 255, 0.28), inset 3px 3px 6px rgba(0, 0, 0, 0.85)',
      light:
        '3px 3px 6px rgba(0, 0, 0, 0.1), -2px -2px 4px rgba(255, 255, 255, 0.8), inset 3px 3px 6px rgba(0, 0, 0, 0.15), inset -3px -3px 6px rgba(255, 255, 255, 0.85)',
    },
  },
  sizes: {
    small: 24,
    medium: 32,
    large: 40,
  },
}
export default neomorphicTheme
