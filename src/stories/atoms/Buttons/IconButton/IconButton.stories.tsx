import { ThemeProvider } from 'styled-components'
import React from 'react'
// also exported from '@storybook/react' if you can deal with breaking changes in 6.1
import { Story, Meta } from '@storybook/react/types-6-0'
import { ReactComponent as Flow } from '../../../assets/flow.svg'
import themeObj from '../../../../core/theme'
import IconButton, { Props } from './IconButton'

export default {
  title: 'Atoms/Buttons/IconButton',
  component: IconButton,
} as Meta

interface StoryProps extends Props {
  darkMode: boolean;
}

const Template: Story<StoryProps> = ({ darkMode, ...args }: StoryProps) => (
  <ThemeProvider theme={{ ...themeObj, style: darkMode ? 'dark' : 'light' }}>
    <IconButton {...args}>
      <Flow />
    </IconButton>
  </ThemeProvider>
)

export const index = Template.bind({})
index.args = {
  variant: 'contained',
  darkMode: false,
}
