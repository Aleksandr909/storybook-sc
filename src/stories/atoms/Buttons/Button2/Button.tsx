import styled from 'styled-components'
import React from 'react'

export interface IButtonProps {
    color?: 'primary' | 'secondary';
}
export interface IButtonProps2 extends React.ButtonHTMLAttributes<HTMLButtonElement>{
    variant?: 'contained' | 'outlined';
}

const Button = styled(({
  children, variant, ...buttonProps
}: IButtonProps2) => (
  <div style={{ width: 'fit-content' }}>
    <button {...buttonProps} type="button">
      {children}
    </button>
  </div>
))`
    border: none;
    outline: none;
    cursor: pointer;
    padding: 15px;
    box-shadow: 0 0 8px #d9d4e7;
    width: 200px;
    height: 65px;
    background-color: #d9d4e7;
    animation-duration: 5s;
    margin-top: 30px;
    text-transform: uppercase;
    div:hover > & {
        box-shadow: inset 0 0 8px #f9f8fc;
        letter-spacing: 5px;
        margin-top: 0;
        margin-bottom: 30px;
        background-color: red;
        & > span {
            background-color: #fec7d7;
        }
    }
    &, &:before {
        position: relative;
        border-radius: 15px;
        box-sizing: border-box;
        transition: all 1.5s ease;
    }
`

export default Button
