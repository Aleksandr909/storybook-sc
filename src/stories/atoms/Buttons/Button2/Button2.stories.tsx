import { ThemeProvider } from 'styled-components'
import React from 'react'
import { Story, Meta } from '@storybook/react/types-6-0'
import themeObj from '../../../../core/theme'
import Button, { IButtonProps } from './Button'

export default {
  title: 'Atoms/Buttons/Button2',
  component: Button,
  parameters: {
    docs: {
      description: {
        component: 'https://codepen.io/Aoo717/pen/GRoGXER',
      },
    },
  },
} as Meta

interface StoryProps extends IButtonProps {
  darkMode: boolean;
  children: React.ReactNode;
}

const Template: Story<StoryProps> = (args) => (
  <ThemeProvider theme={{ ...themeObj, style: args.darkMode ? 'dark' : 'light' }}>
    <Button {...args} />
  </ThemeProvider>
)

export const Primary = Template.bind({})
Primary.args = {
  darkMode: false,
  color: 'primary',
  children: 'button',
}

export const Secondary = Template.bind({})
Secondary.args = {
  darkMode: false,
  color: 'secondary',
  children: 'button',
}
