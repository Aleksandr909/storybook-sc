import styled from 'styled-components'

export interface IButtonProps {
    color?: 'primary' | 'secondary';
}

const Button = styled.button<IButtonProps>`
  padding: 8px 20px;
  outline: none;
  margin-right: 25px;
  cursor: pointer;
  font-size: 18px;
  border-radius: 50px;
  padding: 6px 20px;
  position: relative;
  border: 4px solid transparent;
  background-clip: padding-box;
  transition: .6s all;
  &:hover {
    background-color: transparent;
    color: white;
  }
  &:after {
    transition: .5s all;
    position: absolute;  
    top: -4px;
    left: -4px;
    right: -4px;
    bottom: -4px;
    content: '';
    z-index: -1;
    border-radius: 50px;
    background: linear-gradient(0, rgba(151, 49, 241, 0.46) 0%, rgba(151, 49, 241, 0.46) 6%,rgba(163, 71, 217, 0.46) 6%, rgba(163, 71, 217, 0.46) 33%,rgba(175, 94, 193, 0.46) 33%, rgba(175, 94, 193, 0.46) 47%,rgba(187, 116, 169, 0.46) 47%, rgba(187, 116, 169, 0.46) 65%,rgba(200, 139, 146, 0.46) 65%, rgba(200, 139, 146, 0.46) 77%,rgba(212, 161, 122, 0.46) 77%, rgba(212, 161, 122, 0.46) 87%,rgba(224, 184, 98, 0.46) 87%, rgba(224, 184, 98, 0.46) 97%,rgba(236, 206, 74, 0.46) 97%, rgba(236, 206, 74, 0.46) 100%),linear-gradient(90deg, rgb(233, 14, 27) 0%, rgb(233, 14, 27) 15%,rgb(235, 41, 27) 15%, rgb(235, 41, 27) 39%,rgb(238, 67, 26) 39%, rgb(238, 67, 26) 42%,rgb(240, 94, 26) 42%, rgb(240, 94, 26) 52%,rgb(243, 120, 25) 52%, rgb(243, 120, 25) 61%,rgb(245, 147, 25) 61%, rgb(245, 147, 25) 76%,rgb(248, 173, 24) 76%, rgb(248, 173, 24) 88%,rgb(250, 200, 24) 88%, rgb(250, 200, 24) 100%);
  }
`

export default Button
