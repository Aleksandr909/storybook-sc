import styled from 'styled-components'

export interface IButtonProps {
    color?: 'primary' | 'secondary';
}

const Button = styled.button<IButtonProps>`
  padding: 8px 20px;
  outline: none;
  margin-right: 25px;
  cursor: pointer;
  font-size: 18px;
  border-radius: 50px;
  padding: 6px 20px;
  position: relative;
  border: 4px solid transparent;
  background-clip: padding-box;
  transition: .6s all;
  &:after {
    border-radius: 50px;
    transition: .5s all;
    position: absolute;  
    top: -4px;
    left: -4px;
    right: -4px;
    bottom: -4px;
    content: '';
    z-index: -1;
    background-size: 200% 100%;
    background-position: 0% 0;
    background-image: linear-gradient(135deg, rgb(138, 229, 83) 0%, rgb(138, 229, 83) 22%,rgb(110, 203, 71) 22%, rgb(110, 203, 71) 28%,rgb(83, 176, 59) 28%, rgb(83, 176, 59) 72%,rgb(55, 150, 47) 72%, rgb(55, 150, 47) 100%);
  }
  &:hover {
    background-color: transparent;
    color: white;
    &:after {
      background-position: 100% 0;
    }
  }
`

export default Button
