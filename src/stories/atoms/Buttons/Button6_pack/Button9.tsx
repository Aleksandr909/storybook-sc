import styled from 'styled-components'

export interface IButtonProps {
    color?: 'primary' | 'secondary';
}

const Button = styled.button<IButtonProps>`
  padding: 8px 20px;
  outline: none;
  margin-right: 25px;
  cursor: pointer;
  font-size: 18px;
  border-radius: 4px;
  padding: 6px 20px;
  position: relative;
  border: 4px solid transparent;
  transition: .6s all;
  background-clip: padding-box;
  background-color: transparent;
  color: white;
  &:after {
    transition: .5s all;
    position: absolute;  
    top: -4px;
    left: -4px;
    right: -4px;
    bottom: -4px;
    content: '';
    z-index: -1;
    border-radius: 4px;
    background-size: 200% 100%;
    background-position: 0% 0;
    background-image: linear-gradient(135deg, rgb(13, 4, 178) 0%, rgb(13, 4, 178) 12.5%,rgb(17, 25, 182) 12.5%, rgb(17, 25, 182) 25%,rgb(20, 46, 185) 25%, rgb(20, 46, 185) 37.5%,rgb(24, 67, 189) 37.5%, rgb(24, 67, 189) 50%,rgb(28, 87, 192) 50%, rgb(28, 87, 192) 62.5%,rgb(32, 108, 196) 62.5%, rgb(32, 108, 196) 75%,rgb(35, 129, 199) 75%, rgb(35, 129, 199) 87.5%,rgb(39, 150, 203) 87.5%, rgb(39, 150, 203) 100%);
  }
  &:hover {
    &:after {
      background-position: 100% 0;
      color: white;
    }
  }
`

export default Button
