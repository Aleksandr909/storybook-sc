import styled from 'styled-components'

export interface IButtonProps {
    color?: 'primary' | 'secondary';
}

const Button = styled.button<IButtonProps>`
  padding: 8px 20px;
  outline: none;
  margin-right: 25px;
  cursor: pointer;
  font-size: 18px;
  border-radius: 50px;
  padding: 6px 20px;
  position: relative;
  border: 4px solid transparent;
  background-clip: padding-box;
  transition: .6s all;
  &:after {
    transition: .5s all;
    position: absolute;  
    top: -4px;
    left: -4px;
    right: -4px;
    bottom: -4px;
    content: '';
    z-index: -1;
    border-radius: 50px;
    background-size: 200% 100%;
    background-position: 0% 0;
    background-image: linear-gradient(90deg, hsla(357,85%,70%, 1) 0%, hsla(357,85%,70%, 1) 14.3%,hsla(48.42,85%,70%, 1) 14.3%, hsla(48.42,85%,70%, 1) 28.6%,hsla(99.85,85%,70%, 1) 28.6%, hsla(99.85,85%,70%, 1) 42.9%,hsla(151.28,85%,70%, 1) 42.9%, hsla(151.28,85%,70%, 1) 57.2%,hsla(202.71,85%,70%, 1) 57.2%, hsla(202.71,85%,70%, 1) 71.5%,hsla(254.14,85%,70%, 1) 71.5%, hsla(254.14,85%,70%, 1) 85.8%,hsla(305.57,85%,70%, 1) 85.8%, hsla(305.57,85%,70%, 1) 100.1%);
  }
  &:hover {
    &:after {
      background-position: 100% 0;
    }
  }
`

export default Button
