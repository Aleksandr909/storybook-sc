import styled from 'styled-components'

export interface IButtonProps {
    color?: 'primary' | 'secondary';
}

const Button = styled.button<IButtonProps>`
  padding: 8px 20px;
  outline: none;
  margin-right: 25px;
  cursor: pointer;
  font-size: 18px;
  border-radius: 4px;
  $border-width: 4px;
  color: white;
  border: none;
  background: linear-gradient(45deg, rgba(8, 211, 172, 0.45) 0%, rgba(8, 211, 172, 0.45) 12.5%,rgba(62, 29, 50, 0.45) 12.5%, rgba(62, 29, 50, 0.45) 25%,rgba(54, 55, 67, 0.45) 25%, rgba(54, 55, 67, 0.45) 37.5%,rgba(47, 81, 85, 0.45) 37.5%, rgba(47, 81, 85, 0.45) 50%,rgba(23, 159, 137, 0.45) 50%, rgba(23, 159, 137, 0.45) 62.5%,rgba(16, 185, 155, 0.45) 62.5%, rgba(16, 185, 155, 0.45) 75%,rgba(31, 133, 120, 0.45) 75%, rgba(31, 133, 120, 0.45) 87.5%,rgba(39, 107, 102, 0.45) 87.5%, rgba(39, 107, 102, 0.45) 100%),linear-gradient(135deg, rgb(87, 116, 221),rgb(35, 4, 229));
`

export default Button
