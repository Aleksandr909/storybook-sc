import { ThemeProvider } from 'styled-components'
import React from 'react'
import { Story, Meta } from '@storybook/react/types-6-0'
import themeObj from '../../../../core/theme'
import Button9 from './Button9'
import Button8 from './Button8'
import Button7 from './Button7'
import Button6 from './Button6'
import Button5 from './Button5'
import Button4 from './Button4'
import Button3 from './Button3'
import Button2 from './Button2'
import Button12 from './Button12'
import Button11 from './Button11'
import Button10 from './Button10'
import Button1, { IButtonProps } from './Button1'

export default {
  title: 'Atoms/Buttons/Button6_pack',
  component: Button1,
  parameters: {
    docs: {
      description: {
        component: 'https://codepen.io/TheCSSKing/pen/abowMLW',
      },
    },
  },
} as Meta

interface StoryProps extends IButtonProps {
  darkMode: boolean;
  children: React.ReactNode;
}

const Template: Story<StoryProps> = (args) => (
  <ThemeProvider theme={{ ...themeObj, style: args.darkMode ? 'dark' : 'light' }}>
    <Button1 {...args} />
    <Button2 {...args} />
    <Button3 {...args} />
    <Button4 {...args} />
    <Button5 {...args} />
    <Button6 {...args} />
    <Button7 {...args} />
    <Button8 {...args} />
    <Button9 {...args} />
    <Button10 {...args} />
    <Button11 {...args} />
    <Button12 {...args} />
  </ThemeProvider>
)

export const Primary = Template.bind({})
Primary.args = {
  darkMode: false,
  color: 'primary',
  children: 'button',
}

export const Secondary = Template.bind({})
Secondary.args = {
  darkMode: false,
  color: 'secondary',
  children: 'button',
}
