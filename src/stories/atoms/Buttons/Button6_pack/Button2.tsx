import styled from 'styled-components'

export interface IButtonProps {
    color?: 'primary' | 'secondary';
}

const Button = styled.button<IButtonProps>`
  padding: 8px 20px;
  outline: none;
  margin-right: 25px;
  cursor: pointer;
  font-size: 18px;
  border-radius: 50px;
  padding: 6px 20px;
  position: relative;
  border: 4px solid transparent;
  background-clip: padding-box;
  transition: .5s all;
  &:after {
    background-image: linear-gradient(45deg, rgb(11, 45, 126) 0%, rgb(11, 45, 126) 44%,rgb(21, 87, 153) 44%, rgb(21, 87, 153) 45%,rgb(30, 129, 181) 45%, rgb(30, 129, 181) 61%,rgb(40, 170, 208) 61%, rgb(40, 170, 208) 67%,rgb(49, 212, 235) 67%, rgb(49, 212, 235) 100%);
    border-radius: 50px;
    transition: .5s all;
    position: absolute;  
    top: -4px;
    left: -4px;
    right: -4px;
    bottom: -4px;
    content: '';
    z-index: -1;
  }
`

export default Button
