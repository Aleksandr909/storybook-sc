import styled from 'styled-components'

export interface IButtonProps {
    color?: 'primary' | 'secondary';
}

const Button = styled.button<IButtonProps>`
  padding: 8px 20px;
  outline: none;
  margin-right: 25px;
  cursor: pointer;
  font-size: 18px;
  border-radius: 2px;
  color: white;
  padding: 6px 20px;
  position: relative;
  background-clip: padding-box;
  transition: .6s all;
  border: none;
  background-color: transparent;
  color: white;
  &:after {
    transition: .5s all;
    position: absolute;  
    top: -4px;
    left: -4px;
    right: -4px;
    bottom: -4px;
    content: '';
    z-index: -1;
    border-radius: 4px;
    background-size: 200% 100%;
    background-position: 0% 0;
    background-image: linear-gradient(45deg, rgba(49, 74, 89, 0.45) 0%, rgba(49, 74, 89, 0.45) 12.5%,rgba(122, 86, 72, 0.45) 12.5%, rgba(122, 86, 72, 0.45) 25%,rgba(170, 94, 60, 0.45) 25%, rgba(170, 94, 60, 0.45) 37.5%,rgba(219, 102, 49, 0.45) 37.5%, rgba(219, 102, 49, 0.45) 50%,rgba(146, 90, 66, 0.45) 50%, rgba(146, 90, 66, 0.45) 62.5%,rgba(98, 82, 78, 0.45) 62.5%, rgba(98, 82, 78, 0.45) 75%,rgba(195, 98, 55, 0.45) 75%, rgba(195, 98, 55, 0.45) 87.5%,rgba(73, 78, 83, 0.45) 87.5%, rgba(73, 78, 83, 0.45) 100%),linear-gradient(135deg, rgb(117, 6, 152),rgb(110, 34, 251));
  }
  &:hover {
    background-color: white;
    color: #333;
    &:after {
      background-position: 100% 0;
    }
  }
`

export default Button
