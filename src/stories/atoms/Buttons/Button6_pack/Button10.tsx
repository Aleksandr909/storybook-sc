import styled from 'styled-components'

export interface IButtonProps {
    color?: 'primary' | 'secondary';
}

const Button = styled.button<IButtonProps>`
  padding: 8px 20px;
  outline: none;
  margin-right: 25px;
  cursor: pointer;
  font-size: 18px;
  border-radius: 4px;
  padding: 6px 20px;
  position: relative;
  border: 4px solid transparent;
  background-clip: padding-box;
  transition: .6s all;
  &:after {
    transition: .5s all;
    position: absolute;  
    top: -4px;
    left: -4px;
    right: -4px;
    bottom: -4px;
    content: '';
    z-index: -1;
    border-radius: 4px;
    background-size: 200% 100%;
    background-position: 0% 0;
    background-image: linear-gradient(135deg, rgb(246, 137, 24) 0%, rgb(246, 137, 24) 31%,rgb(174, 62, 159) 31%, rgb(174, 62, 159) 42%,rgb(210, 100, 92) 42%, rgb(210, 100, 92) 49%,rgb(138, 25, 227) 49%, rgb(138, 25, 227) 100%);
  }
  &:hover {
    background-color: transparent;
    color: white;
    &:after {
      background-position: 100% 0;
    }
  }
`

export default Button
