import styled from 'styled-components'

export interface IButtonProps {
    color?: 'primary' | 'secondary' | 'default';
    fullWidth?: boolean;
}

const Button = styled.button<IButtonProps>`
    user-select: none;
    -webkit-tap-highlight-color: transparent;
    height: 50px;
    padding: 4px 16px;
    cursor: pointer;
    border: none;
    color: ${({ theme }) => theme.colors.antiTextPrimary[theme.style]};
    font-size: 24px;
    border-radius: 4px;
    background: ${({ color = 'default', theme }) => theme.colors[color][theme.style]};
    box-shadow: inset 0 10px 10px rgba(255,255,255,0.1), inset 0 -10px 10px rgba(0,0,0,0.1);
    transition: 1s;
    text-transform: uppercase;
    width: ${({ fullWidth }) => fullWidth && '100%'};
    &:hover {
        box-shadow: inset 0 -10px 10px rgba(255,255,255,0.1), inset 0 10px 10px rgba(0,0,0,0.1);
    }
    &:focus {
        outline: none;
    }
`

export default Button
