/* eslint-disable no-unused-expressions */
import styled from 'styled-components'
import React, { HTMLAttributes } from 'react'

export interface Props extends HTMLAttributes<HTMLButtonElement> {
    color?: 'primary' | 'secondary' | 'default';
    fullWidth?: boolean;
    variant?: 'outlined' | 'contained';
}

const ButtonStyled = styled.button<Props>`
  --col-primary: ${({ color = 'default', theme }) => theme.colors[color][theme.style]};
  --bg: ${({ variant }) => (variant === 'contained' ? 'var(--col-primary)' : 'rgba(255,255,255,0.8)')};
  --color: ${({ theme, variant }) => (variant === 'contained' ? theme.colors.body[theme.style] : 'var(--col-primary)')};
  --x: 50%;
  --y: 50%;
  position: relative;
  appearance: none;
  padding: 1em 2em;
  color: var(--color);
  cursor: pointer;
  outline: none;
  border-radius: 100px;
  border: 2px solid transparent;
  background: linear-gradient(var(--bg), var(--bg)) padding-box, radial-gradient(farthest-corner at var(--x) var(--y), #00C9A7, #845EC2) border-box;
`

const Button = (props: Props) => {
  const mouseMoveHandler = (e: React.MouseEvent<HTMLButtonElement>) => {
    const x = e.pageX - e.clientY
    const y = e.pageY - e.clientX
    e.currentTarget.style.setProperty('--x', `${x}px`)
    e.currentTarget.style.setProperty('--y', `${y}px`)
  }

  return (
    <ButtonStyled {...props} onMouseMove={mouseMoveHandler} />
  )
}

export default Button
