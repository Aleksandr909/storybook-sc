/* eslint-disable jsx-a11y/mouse-events-have-key-events */
import styled from 'styled-components'
import React, { useRef } from 'react'

export interface IButtonProps extends React.ButtonHTMLAttributes<HTMLButtonElement>{
    variant?: 'contained' | 'outlined';
    color?: 'primary' | 'secondary' | 'default';
    fullWidth?: boolean;
}

const Button = styled(({
  children, variant, ...props
}: IButtonProps) => {
  const animBtn = useRef<null | HTMLButtonElement>(
    null,
  ) as React.MutableRefObject<HTMLButtonElement>
  const spanRef = useRef<null | HTMLSpanElement>(null) as React.MutableRefObject<HTMLSpanElement>
  const mouseOverHandler = (e: React.MouseEvent<HTMLSpanElement>) => {
    spanRef.current.style.left = `${e.pageX - animBtn.current.offsetLeft}px`
    spanRef.current.style.top = `${e.pageY - animBtn.current.offsetTop}px`
  }
  const mouseOutHandler = (e: React.MouseEvent<HTMLSpanElement>) => {
    spanRef.current.style.left = `${e.pageX - animBtn.current.offsetLeft}px`
    spanRef.current.style.top = `${e.pageY - animBtn.current.offsetTop}px`
  }
  return (
    <button
      type="button"
      {...props}
      ref={animBtn}
      onMouseOver={mouseOverHandler}
      onMouseOut={mouseOutHandler}
    >
      <span
        ref={spanRef}
      />
      <p>{children}</p>
    </button>
  )
})`
    outline: 0;
    display: flex;
    align-items: center;
    --width: 100%;
    --time: 0.7s;
    width: ${({ fullWidth }) => fullWidth && '100%'};
    cursor: pointer;
    position: relative;
    height: 32px;
    padding: 0 20px;
    color: ${({ variant = 'contained', color = 'primary', theme }) => (variant === 'outlined'
    ? theme.colors[color][theme.style]
    : theme.colors.antiTextPrimary[theme.style])};
    background: ${({ variant = 'contained', color = 'primary', theme }) => (variant === 'contained'
    ? theme.colors[color][theme.style]
    : 'none')};
    border: ${({ variant = 'contained', color = 'primary', theme }) => (variant === 'outlined'
    ? `1px solid ${theme.colors[color][theme.style]}`
    : 0)};
    overflow: hidden;
    border-radius: 4px;
    transition: background 0.7s ease 0.1s;
    & p {
        z-index: 5;
        line-height: 1;
        margin: 0 auto;
    };
    &:hover {
      & > span {
        width: calc(var(--width) * 2.5);
        padding-top: calc(var(--width) * 2.5);
      }
    };
    & span {
      border-radius: 100%;
      position: absolute;
      display: block;
      z-index: 0;
      width: 0;
      height: 0;
      background: #000;
      opacity: 0.2;
      transform: translate(-50%, -50%);
      transition: width var(--time), padding-top var(--time);
    };
    &.animated {
        --angle: 6deg;
        animation: shake 0.3s;
    };
    @keyframes shake {
        25%: {
        transform: rotate(calc(var(--angle) * -1));
        };
        50%: {
        transform: rotate(var(--angle));
        };
        100%: {
        transform: rotate(0deg);
        };
    };
`

export default Button
