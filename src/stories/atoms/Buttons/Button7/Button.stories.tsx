import { ThemeProvider } from 'styled-components'
import React from 'react'
// also exported from '@storybook/react' if you can deal with breaking changes in 6.1
import { Story, Meta } from '@storybook/react/types-6-0'
import themeObj from '../../../../core/theme'
import Button, { IButtonProps } from './Button'

export default {
  title: 'Atoms/Buttons/Button7',
  component: Button,
} as Meta

interface StoryProps extends IButtonProps {
  darkMode: boolean;
}

const Template: Story<StoryProps> = ({ darkMode, ...args }: StoryProps) => (
  <ThemeProvider theme={{ ...themeObj, style: darkMode ? 'dark' : 'light' }}>
    <Button {...args} />
  </ThemeProvider>
)

export const index = Template.bind({})
index.args = {
  variant: 'contained',
  children: 'Button',
  darkMode: false,
}
