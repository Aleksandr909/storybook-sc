import styled from 'styled-components'
import React from 'react'

export interface Props extends React.ButtonHTMLAttributes<HTMLButtonElement>{
    variant?: 'contained' | 'outlined';
}

const ButtonStyled = styled.button<Props>`
  --color: #fff;
  --color-hover: #2B3044;
  --background: #362A89;
  --background-hover: var(--background);
  --hover-back: #6D58FF;
  --hover-front: #F6F8FF;
  padding: 8px 28px;
  border-radius: 20px;
  line-height: 24px;
  font-size: 14px;
  font-weight: 600;
  letter-spacing: .02em;
  border: none;
  outline: none;
  position: relative;
  overflow: hidden;
  cursor: pointer;
  -webkit-appearance: none;
  -webkit-tap-highlight-color: transparent;
  -webkit-mask-image: -webkit-radial-gradient(white, black);
  color: var(--c, var(--color));
  background: var(--b, var(--background));
  transition: color .2s linear var(--c-d, .2s), background .3s linear var(--b-d, .2s);
  &:not(.simple) {
    &:before,
    &:after {
      content: '';
      position: absolute;
      background: var(--pb, var(--hover-back));
      top: 0;
      left: 0;
      right: 0;
      height: 200%;
      border-radius: var(--br, 40%);
      transform: translateY(var(--y, 50%));
      transition: transform var(--d, .4s) ease-in var(--d-d, 0s), border-radius .5s ease var(--br-d, .08s);
    }
    &:after {
      --pb: var(--hover-front);
      --d: .44s;
    }
  }
  div {
    z-index: 1;
    position: relative;
    display: flex;
    span {
      display: block;
      backface-visibility: hidden;
      transform: translateZ(0);
      animation: var(--name, none) .7s linear forwards .18s;
    }
  }
  &.in {
    --name: move;
    &:not(.out) {
      --c: var(--color-hover);
      --b: var(--background-hover);
      &:before,
      &:after {
        --y: 0;
        --br: 5%;
      }
      &:after {
        --br: 10%;
        --d-d: .02s;
      }
    }
    &.out {
      --name: move-out;
      &:before {
        --d-d: .06s;
      }
    }
  }
  @keyframes move {
    30%, 36% {
      transform: translateY(calc(-6px * var(--move))) translateZ(0) rotate(calc(-13deg * var(--rotate) * var(--part)));
    }
    50% {
      transform: translateY(calc(3px * var(--move))) translateZ(0) rotate(calc(6deg * var(--rotate) * var(--part)));
    }
    70% {
      transform: translateY(calc(-2px * var(--move))) translateZ(0) rotate(calc(-3deg * var(--rotate) * var(--part)));
    }
  }
  @keyframes move-out {
    30%, 36% {
      transform: translateY(calc(6px * var(--move))) translateZ(0) rotate(calc(13deg * var(--rotate) * var(--part)));
    }
    50% {
      transform: translateY(calc(-3px * var(--move))) translateZ(0) rotate(calc(-6deg * var(--rotate) * var(--part)));
    }
    70% {
      transform: translateY(calc(2px * var(--move))) translateZ(0) rotate(calc(3deg * var(--rotate) * var(--part)));
    }
  }
`

const Button: React.FC<Props> = ({ onMouseEnter, onMouseLeave, ...props }) => {
  const btn = React.useRef<HTMLButtonElement>() as React.MutableRefObject<HTMLButtonElement>
  React.useEffect(() => {
    const div = document.createElement('div')
    const letters = ((btn.current as HTMLButtonElement).textContent as string).trim().split('')

    function elements(letter: string, index: number, array: string[]) {
      const element = document.createElement('span')
      const part = (index >= array.length / 2) ? -1 : 1
      const position = (index >= array.length / 2)
        ? array.length / 2 - index + (array.length / 2 - 1)
        : index
      const move = position / (array.length / 2)
      const rotate = 1 - move

      element.innerHTML = !letter.trim() ? '&nbsp;' : letter
      element.style.setProperty('--move', move.toString())
      element.style.setProperty('--rotate', rotate.toString())
      element.style.setProperty('--part', part.toString())

      div.appendChild(element)
    }

    letters.forEach(elements)

    btn.current.innerHTML = div.outerHTML
  }, [btn])

  const mouseenterHandler = (e: React.MouseEvent<HTMLButtonElement>) => {
    if (!btn.current.classList.contains('out')) {
      btn.current.classList.add('in')
    }
    if (onMouseEnter) onMouseEnter(e)
  }

  const mouseleaveHandler = (e: React.MouseEvent<HTMLButtonElement>) => {
    if (btn.current.classList.contains('in')) {
      btn.current.classList.add('out')
      setTimeout(() => btn.current.classList.remove('in', 'out'), 950)
    }
    if (onMouseLeave) onMouseLeave(e)
  }
  return (
    <ButtonStyled
      {...props}
      ref={btn}
      onMouseEnter={mouseenterHandler}
      onMouseLeave={mouseleaveHandler}
    />
  )
}

export default Button
