import { ThemeProvider } from 'styled-components'
import React from 'react'
import { Story, Meta } from '@storybook/react/types-6-0'
import themeObj from '../../../../core/theme'
import Button3 from './Button3'
import Button2 from './Button2'
import Button1, { Props } from './Button1'

export default {
  title: 'Atoms/Buttons/Button5',
  component: Button1,
  parameters: {
    docs: {
      description: {
        component: 'https://codepen.io/aaroniker/pen/OJPqPMR',
      },
    },
  },
} as Meta

interface StoryProps extends Props {
  darkMode: boolean;
  children: React.ReactNode;
}

const Template: Story<StoryProps> = (args) => (
  <ThemeProvider theme={{ ...themeObj, style: args.darkMode ? 'dark' : 'light' }}>
    <Button1 {...args} />
    <Button2 {...args} />
    <Button3 {...args} />
  </ThemeProvider>
)

export const Primary = Template.bind({})
Primary.args = {
  darkMode: false,
  children: 'button',
}

export const Secondary = Template.bind({})
Secondary.args = {
  darkMode: false,
  children: 'button',
}
