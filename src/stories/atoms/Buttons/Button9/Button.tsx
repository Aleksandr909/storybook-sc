/* eslint-disable no-unused-expressions */
import styled from 'styled-components'
import React, { HTMLAttributes } from 'react'

export interface Props extends HTMLAttributes<HTMLButtonElement> {
    color?: 'primary' | 'secondary' | 'default';
    fullWidth?: boolean;
    variant?: 'outlined' | 'contained';
}

const ButtonStyled = styled.button<Props>`
  --col-primary: ${({ color = 'default', theme }) => theme.colors[color][theme.style]};
  --color: ${({ theme, variant }) => (variant === 'contained' ? theme.colors.body[theme.style] : 'var(--col-primary)')};
  --ripple-opacity: 0.3;  
  --ripple-duration: 600ms;     
  border: 1px solid var(--col-primary);
  outline: none;
  border-radius: 4px;
  padding: 0.75em 3em;
  font-size: 1em;
  display:block;
  background: ${({ variant }) => (variant === 'contained' ? 'var(--col-primary)' : 'rgba(0,0,0,0)')};
  color: var(--color);
  -webkit-tap-highlight-color: rgba(0,0,0,0);
  min-width: 200px;
  text-align: center; 
  cursor: pointer;
  user-select: none;
  position: relative;
  overflow: hidden;
  &:before {
    content: '';
    position:absolute;
    display: block;
    background: var(--color, white);      
    border-radius: 50%;
    pointer-events: none;
    top: calc(var(--y) * 1px);
    left: calc(var(--x) * 1px);
    width:  calc(var(--d) * 1px);
    height: calc(var(--d) * 1px);
    opacity: calc(var(--o, 1) * var(--ripple-opacity, 0.3));                
    transition: calc(var(--t, 0) * var(--ripple-duration, 600ms)) var(--ripple-easing,linear);   
    transform: translate(-50%, -50%) scale(var(--s, 1));
    transform-origin: center;
  }
`

const Button = (props: Props) => {
  const onClickHandler = (e: React.MouseEvent<HTMLButtonElement>) => {
    if (props.onClick) props.onClick(e)
    const el = e.currentTarget
    const r = el.getBoundingClientRect()
    const d = Math.sqrt(r.width ** 2 + r.height ** 2) * 2
    el.style.cssText = '--s: 0; --o: 1;'
    const x = e.clientX
    const y = e.clientY
    setTimeout(() => {
      el.style.cssText = `--t: 1; --o: 0; --d: ${d}; --x:${x - r.left}; --y:${y - r.top};`
    }, 0)
  }

  return (
    <ButtonStyled {...props} onClick={onClickHandler} />
  )
}

export default Button
