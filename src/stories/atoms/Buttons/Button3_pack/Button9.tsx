import styled from 'styled-components'

export interface IButtonProps {
    color?: 'primary' | 'secondary';
}

const Button = styled.button<IButtonProps>`
  padding: 10px 25px;
  outline: none;
  font-family: 'Lato', sans-serif;
  font-weight: 500;
  background: transparent;
  cursor: pointer;
  transition: all 0.3s ease;
  position: relative;
  display: inline-block;
  border: none;
  z-index: 2;
  transition: all 0.3s ease;
  overflow: hidden;
  &:after {
    position: absolute;
    content: " ";
    z-index: -1;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    transition: all 0.3s ease;
  }
  &:hover {
    box-shadow:  
      4px 4px 6px 0 rgba(255,255,255,.5),
      -4px -4px 6px 0 rgba(116, 125, 136, .2), 
      inset -4px -4px 6px 0 rgba(255,255,255,.5),
      inset 4px 4px 6px 0 rgba(116, 125, 136, .3);
    color: #000;
    &:after {
      transform: scale(2) rotate(180deg);
      box-shadow: 
        4px 4px 6px 0 rgba(255,255,255,.5),
        -4px -4px 6px 0 rgba(116, 125, 136, .2), 
        inset -4px -4px 6px 0 rgba(255,255,255,.5),
        inset 4px 4px 6px 0 rgba(116, 125, 136, .3);
    }
  }
`

export default Button
