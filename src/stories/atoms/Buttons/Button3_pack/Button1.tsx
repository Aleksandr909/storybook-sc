import styled from 'styled-components'

export interface IButtonProps {
    color?: 'primary' | 'secondary';
}

const Button = styled.button<IButtonProps>`
    padding: 10px 25px;
    outline: none;
    font-family: 'Lato', sans-serif;
    font-weight: 500;
    background: transparent;
    cursor: pointer;
    transition: all 0.3s ease;
    position: relative;
    display: inline-block;
    border: none;
    &:hover {
       box-shadow:
        -7px -7px 20px 0px #fff9,
        -4px -4px 5px 0px #fff9,
        7px 7px 20px 0px #0002,
        4px 4px 5px 0px #0001;
    }
`

export default Button
