import styled from 'styled-components'
import React from 'react'

export interface IButtonProps extends React.ButtonHTMLAttributes<HTMLButtonElement> {
    color?: 'primary' | 'secondary';
}

const Button = styled(({
  children, ...buttonProps
}: IButtonProps) => (
  <button {...buttonProps} type="button">
    <span>
      {children}
    </span>
  </button>
))`
  outline: none;
  font-family: 'Lato', sans-serif;
  font-weight: 500;
  background: transparent;
  cursor: pointer;
  transition: all 0.3s ease;
  position: relative;
  display: inline-block;
  border: none;
  width: 130px;
  height: 40px;
  line-height: 42px;
  padding: 0;
  &:before, &:after {
    position: absolute;
    content: "";
    height: 0%;
    width: 2px;
    box-shadow:
      -7px -7px 20px 0px rgba(255,255,255,.9),
      -4px -4px 5px 0px rgba(255,255,255,.9),
      7px 7px 20px 0px rgba(0,0,0,.2),
      4px 4px 5px 0px rgba(0,0,0,.3);
  }
  &:before {
    right: 0;
    top: 0;
    transition: all 500ms ease;
  }
  &:after {
    left: 0;
    bottom: 0;
    transition: all 500ms ease;
  }
  &:hover:before {
    transition: all 500ms ease;
    height: 100%;
  }
  &:hover:after {
    transition: all 500ms ease;
    height: 100%;
  }
  & span {
    position: relative;
    display: block;
    width: 100%;
    height: 100%;
    &:before, &:after {
      position: absolute;
      content: "";
      box-shadow:
        -7px -7px 20px 0px rgba(255,255,255,.9),
        -4px -4px 5px 0px rgba(255,255,255,.9),
        7px 7px 20px 0px rgba(0,0,0,.2),
        4px 4px 5px 0px rgba(0,0,0,.3);
    }
    &:before {
      left: 0;
      top: 0;
      width: 0%;
      height: 2px;
      transition: all 500ms ease;
    }
    &:after {
      right: 0;
      bottom: 0;
      width: 0%;
      height: 2px;
      transition: all 500ms ease;
    }
    &:hover:before {
      width: 100%;
    }
    &:hover:after {
      width: 100%;
    }
  }
`

export default Button
