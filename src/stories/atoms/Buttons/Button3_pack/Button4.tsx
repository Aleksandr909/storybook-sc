import styled from 'styled-components'
import React from 'react'

export interface IButtonProps extends React.ButtonHTMLAttributes<HTMLButtonElement> {
    color?: 'primary' | 'secondary';
}

const Button = styled(({
  children, ...buttonProps
}: IButtonProps) => (
  <button {...buttonProps} type="button">
    <span>
      {children}
    </span>
  </button>
))`
  outline: none;
  font-family: 'Lato', sans-serif;
  font-weight: 500;
  background: transparent;
  cursor: pointer;
  transition: all 0.3s ease;
  position: relative;
  display: inline-block;
  border: none;
  width: 130px;
  height: 40px;
  line-height: 42px;
  padding: 0;
  &:before, &:after {
    position: absolute;
    content: "";
    right: 0;
    top: 0;
    box-shadow:  
      4px 4px 6px 0 rgba(255,255,255,.5),
      -4px -4px 6px 0 rgba(116, 125, 136, .2), 
      inset -4px -4px 6px 0 rgba(255,255,255,.5),
      inset 4px 4px 6px 0 rgba(116, 125, 136, .3);
    transition: all 0.3s ease;
  }
  &:before {
    height: 0%;
    width: 2px;
  }
  &:after {
    width: 0%;
    height: 2px;
  }
  &:hover:before {
    height: 100%;
  }
  &:hover:after {
    width: 100%;
  }
  & span {
    position: relative;
    display: block;
    width: 100%;
    height: 100%;
    &:before, &:after {
      position: absolute;
      content: "";
      left: 0;
      bottom: 0;
      box-shadow:  
        4px 4px 6px 0 rgba(255,255,255,.5),
        -4px -4px 6px 0 rgba(116, 125, 136, .2), 
        inset -4px -4px 6px 0 rgba(255,255,255,.5),
        inset 4px 4px 6px 0 rgba(116, 125, 136, .3);
      transition: all 0.3s ease;
    }
    &:before {
      width: 2px;
      height: 0%;
    }
    &:after {
      width: 0%;
      height: 2px;
    }
    &:hover:before {
      height: 100%;
    }
    &:hover:after {
      width: 100%;
    }
  }
`

export default Button
