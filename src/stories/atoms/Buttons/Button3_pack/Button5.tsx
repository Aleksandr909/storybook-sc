import styled from 'styled-components'

export interface IButtonProps {
    color?: 'primary' | 'secondary';
}

const Button = styled.button<IButtonProps>`
  font-family: 'Lato', sans-serif;
  font-weight: 500;
  outline: none;
  background: transparent;
  cursor: pointer;
  transition: all 0.3s ease;
  position: relative;
  display: inline-block;
  border: none;
  width: 130px;
  height: 40px;
  line-height: 42px;
  padding: 0;
  &:hover {
     box-shadow:
      -7px -7px 20px 0px #fff9,
      -4px -4px 5px 0px #fff9,
      7px 7px 20px 0px #0002,
      4px 4px 5px 0px #0001;
     &:before, &:after {
       width:100%;
       transition: .8s ease all;
     }
  }
  &:before, &:after {
    content:"";
    position:absolute;
    top:0;
    right:0;
    height:2px;
    width:0;
    box-shadow:
      -7px -7px 20px 0px #fff,
      -4px -4px 5px 0px #fff,
      7px 7px 20px 0px #0003,
      4px 4px 5px 0px #0002;
    transition:.4s ease all;
  }
  &:after {
    right: inherit;
    top: inherit;
    left: 0;
    bottom: 0;
  }
`

export default Button
