import styled from 'styled-components'
import React from 'react'

export interface IButtonProps extends React.ButtonHTMLAttributes<HTMLButtonElement> {
    color?: 'primary' | 'secondary';
}

const Button = styled(({
  children, ...buttonProps
}: IButtonProps) => (
  <button {...buttonProps} type="button">
    <span>
      {children}
    </span>
    <span>
      {children}
      2
    </span>
  </button>
))`
  padding: 10px 25px;
  font-family: 'Lato', sans-serif;
  font-weight: 500;
  outline: none;
  background: transparent;
  cursor: pointer;
  transition: all 0.3s ease;
  position: relative;
  display: inline-block;
  border: none;
  left: -20px;
  top: -20px;
  margin-top: 20px;
  width: 130px;
  height: 40px;
  line-height: 42px;
  perspective: 230px;
  & span {
    display: block;
    position: absolute;
    width: 130px;
    height: 40px;
    border: none;
    margin:0;
    text-align: center;
    box-sizing: border-box;
    transition: all .3s;
    &:nth-child(1) {
      box-shadow:
        -7px -7px 20px 0px #fff9,
        -4px -4px 5px 0px #fff9,
        7px 7px 20px 0px #0002,
        4px 4px 5px 0px #0001;
      transform: rotateX(90deg);
      transform-origin: 50% 50% -20px;
    }
    &:nth-child(2) {
      transform: rotateX(0deg);
      transform-origin: 50% 50% -20px;
    }
  }
  &:hover span:nth-child(1) {
    transform: rotateX(0deg);
  }
  &:hover span:nth-child(2) {
    opacity: 0;
    background: #e0e5ec;
    color: #e0e5ec;
    transform: rotateX(-90deg);
  }
`

export default Button
