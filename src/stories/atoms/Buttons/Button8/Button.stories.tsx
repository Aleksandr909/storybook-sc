import { ThemeProvider } from 'styled-components'
import React from 'react'
import { Story, Meta } from '@storybook/react/types-6-0'
import themeObj from '../../../../core/theme'
import Button, { IButtonProps } from './Button'

export default {
  title: 'Atoms/Buttons/Button8',
  component: Button,
  parameters: {
    docs: {
      description: {
        component: 'https://codepen.io/nw/pen/GqBzJ',
      },
    },
  },
  argTypes: {
    color: {
      control: {
        type: 'select',
        options: ['primary', 'secondary', 'default'],
      },
    },
  },
} as Meta

interface StoryProps extends IButtonProps {
  darkMode: boolean;
  children: React.ReactNode;
}

const Template: Story<StoryProps> = ({ darkMode, ...args }: StoryProps) => (
  <ThemeProvider theme={{ ...themeObj, style: darkMode ? 'dark' : 'light' }}>
    <Button {...args} />
  </ThemeProvider>
)

export const index = Template.bind({})
index.args = {
  darkMode: false,
  children: 'button',
}
