import styled from 'styled-components'

export interface IButtonProps {
    color?: 'primary' | 'secondary' | 'default';
    fullWidth?: boolean;
}

const Button = styled.button<IButtonProps>`
  --col-primary: ${({ color = 'default', theme }) => theme.colors[color][theme.style]};
  --corner-radius: 4px;
  cursor: pointer;
  text-decoration: none;
  text-transform: uppercase;
  font-family: 'Exo 2', sans-serif;
  font-weight: 300;
  font-size: 30px;
  display: inline-block;
  position: relative;
  text-align: center;
  color: var(--col-primary);
  border: 1px solid var(--col-primary);
  border-radius: var(--corner-radius);
  line-height: 3em;
  padding-left: 5em;
  padding-right: 5em;
  box-shadow: 0 0 0 0 transparent;
  transition: all 0.2s ease-in;
  background-color: rgba(255,255,255, 0);
  &:hover {
    color: white;
    box-shadow: 0 0 6px 0 var(--col-primary);
    background-color: var(--col-primary);
    transition: all 0.2s ease-out;
    &:before {
      animation: shine 0.5s 0s linear;
    }
  }
  &:active {
    box-shadow: 0 0 0 0 transparent;
    transition: box-shadow 0.2s ease-in;
  }
  &:before {
    content: '';
    display: block;
    width: 0px;
    height: 86%;
    position: absolute;
    top: 7%;
    left: 0%;
    opacity: 0;
    box-shadow: 0 0 15px 3px white;
    transform: skewX(-20deg);
  }
  @keyframes shine {
    from {
      opacity: 0;
      left: 0%;
    }
    50% {
      opacity: 1;
    }
    to {
      opacity: 0;
      left: 100%;
    }
  }  
`

export default Button
