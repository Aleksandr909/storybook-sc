import styled from 'styled-components'
import React from 'react'

export interface IButtonProps extends React.ButtonHTMLAttributes<HTMLButtonElement>{
  color?: 'primary' | 'secondary' | 'default';
  fullWidth?: boolean;
}

const Button = styled(({
  children, color, fullWidth, ...props
}: IButtonProps) => (
  <button type="button" {...props}>
    <span>{children}</span>
    <div className="wave" />
  </button>
))`
  cursor: pointer;
  width: 200px;
  padding: 20px 30px;
  position: relative;
  display: block;
  text-decoration: none;
  overflow: hidden;
  border: none;
  outline: none;
  text-transform: uppercase;
  &:hover {
    .wave {
      top: -120px;
    }
  }
  & > span {
    position: relative;
    z-index: 1;
    color: ${({ theme }) => theme.colors.antiTextPrimary[theme.style]};
    font-size: 15px;
    letter-spacing: 8px;
  }
  & > .wave {
    width: 200px;
    height: 200px;
    background-color: ${({ color = 'default', theme }) => theme.colors[color][theme.style]};
    box-shadow: inset 0 0 50px rgba(0,0,0,.5);
    position: absolute;
    left: 0;
    top: -80px;
    transition: 0.4s;
    &::before, &::after {
      width: 200%;
      height: 200%;
      content: '';
      position: absolute;
      top: 0;
      left: 50%;
      transform: translate(-50%,-75%);
    }
    &::before {
      border-radius: 45%;
      background-color: ${({ color = 'default', theme }) => theme.colors[color][theme.style]};
      animation: wave 5s linear infinite;
    }
    &::after {
      border-radius: 40%;
      background-color: rgba(20,20,20,0.5);
      animation: wave 10s linear infinite;
    }
  }
  @keyframes wave {
    0% {
      transform: translate(-50%,-75%) rotate(0deg);
    }
    100% {
      transform: translate(-50%,-75%) rotate(360deg);
    }
  }
`

export default Button
