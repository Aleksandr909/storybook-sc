import styled from 'styled-components'
import * as React from 'react'
import { ReactComponent as Telegram } from '../telegram.svg'
import { ReactComponent as Gmail } from '../gmail.svg'
import { ReactComponent as Gitlab } from '../gitlab.svg'
import { ReactComponent as Github } from '../github.svg'

export interface Props extends React.HTMLAttributes<HTMLDivElement> {
  color?: 'primary' | 'secondary' | 'default';
}

const SocialContainer = styled.div<Props>`
    & > a > svg {
        width: 48px;
        height: 48px;
        transition: all 0.2s ease;
        fill: ${({ theme, color = 'default' }) => theme.colors[color][theme.style]};
        opacity: 0.8;
        @media(max-width: 960px) {
            width: 32px;
            height: 32px;
        };
        @media(max-width: 600px) {
            width: 24px;
            height: 24px;
        };
    };
    &:hover {
        & > a > svg {
            fill: ${({ theme, color = 'default' }) => theme.colors[color][theme.style]};
            opacity: 0.6;
        }
    };
    & > a {
        margin: 0 4px;
        & > svg {
            &:hover {
                fill: ${({ theme, color = 'default' }) => theme.colors[color][theme.style]};
                opacity: 1;
                filter: ${({ theme, color = 'default' }) => `drop-shadow(0 0 4px ${theme.colors[color][theme.style]})`};
            };
        }
    };
`

const Social: React.FunctionComponent<Props> = (props: Props) => (
  <SocialContainer {...props}>
    <a href="mailto: sasha2822222@gmail.com">
      <Gmail />
    </a>
    <a href="https://t.me/vordgi" target="_blank" rel="noopener noreferrer">
      <Telegram />
    </a>
    <a href="https://github.com/Aleksandr909" target="_blank" rel="noopener noreferrer">
      <Github />
    </a>
    <a href="https://gitlab.com/Aleksandr909" target="_blank" rel="noopener noreferrer">
      <Gitlab />
    </a>
  </SocialContainer>
)

export default Social
