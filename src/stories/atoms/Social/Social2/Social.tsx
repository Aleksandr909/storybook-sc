import styled from 'styled-components'
import * as React from 'react'
import { ReactComponent as Telegram } from '../telegram.svg'
import { ReactComponent as Gmail } from '../gmail.svg'
import { ReactComponent as Gitlab } from '../gitlab.svg'
import { ReactComponent as Github } from '../github.svg'

export interface Props extends React.HTMLAttributes<HTMLDivElement> {
  color?: 'primary' | 'secondary' | 'default';
}

const SocialContainer = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-around;
  align-items: center;
  height: 80px;
  width: 350px;
  position: relative;
  background-color: rgba(144, 144, 144, 0.1);
  box-shadow:
      -7px -7px 20px 0px #fff9,
      -4px -4px 5px 0px #fff9,
      7px 7px 20px 0px #0002,
      4px 4px 5px 0px #0001,
      inset 0px 0px 0px 0px #fff9,
      inset 0px 0px 0px 0px #0001;
  border-radius: 10px;
  & > a {
    transition: all .3s ease;
    box-sizing: border-box;
    height: 36px;
    width: 36px;
    padding: 4px;
    border-radius: 3px;
    background: #e0e5ec;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    -webkit-tap-highlight-color: rgba(0,0,0,0);
    -webkit-tap-highlight-color: transparent;
    box-shadow:
      -7px -7px 20px 0px #fff9,
      -4px -4px 5px 0px #fff9,
      7px 7px 20px 0px #0002,
      4px 4px 5px 0px #0001,
      inset 0px 0px 0px 0px #fff9,        
      inset 0px 0px 0px 0px #0001;
    font-size: 16px;
    text-decoration: none;
    & > svg {
      fill: rgba(42, 52, 84, 1);
    }
    &:hover{
      padding: 5px;
      box-shadow:  
        4px 4px 6px 0 rgba(255,255,255,.5),
        -4px -4px 6px 0 rgba(116, 125, 136, .2), 
        inset -4px -4px 6px 0 rgba(255,255,255,.5),
        inset 4px 4px 6px 0 rgba(116, 125, 136, .3);
    }
  }
`

const Social: React.FunctionComponent<Props> = (props: Props) => (
  <SocialContainer {...props}>
    <a href="mailto: sasha2822222@gmail.com">
      <Gmail />
    </a>
    <a href="https://t.me/vordgi" target="_blank" rel="noopener noreferrer">
      <Telegram />
    </a>
    <a href="https://github.com/Aleksandr909" target="_blank" rel="noopener noreferrer">
      <Github />
    </a>
    <a href="https://gitlab.com/Aleksandr909" target="_blank" rel="noopener noreferrer">
      <Gitlab />
    </a>
  </SocialContainer>
)

export default Social
