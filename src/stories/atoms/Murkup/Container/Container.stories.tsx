import React from 'react'
// also exported from '@storybook/react' if you can deal with breaking changes in 6.1
import { Story, Meta } from '@storybook/react/types-6-0'
import Container from './Container'

export default {
  title: 'Atoms/Markup/Container',
  component: Container,
} as Meta

interface StoryProps {
  style: {[key: string]: string};
}

const Template: Story<StoryProps> = (args) => (
  <Container {...args} />
)

export const index = Template.bind({})
index.args = {
  style: { border: '1px solid #eee', height: '100px' },
}
