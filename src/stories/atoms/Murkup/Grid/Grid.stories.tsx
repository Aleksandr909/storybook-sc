import React from 'react'
// also exported from '@storybook/react' if you can deal with breaking changes in 6.1
import { Story, Meta } from '@storybook/react/types-6-0'
import Grid, { IGridProps } from './Grid'

export default {
  title: 'Atoms/Markup/Grid',
  component: Grid,
  argTypes: {
    xs: { description: 'Размер при экране до 600px (100%/xs)' },
    sm: { description: 'Размер при экране от 600px до 960px (100%/sm)' },
    md: { description: 'Размер при экране от 960px до 1280px (100%/md)' },
    lg: { description: 'Размер при экране от 1280px до 1920px (100%/lg)' },
    xl: { description: 'Размер при экране от 1920px (100%/xl)' },
    container: { description: 'Является ли элемент контейнером (display: flex)' },
  },
} as Meta

interface StoryProps extends IGridProps {
  style: {[key: string]: string};
}

const Template: Story<StoryProps> = (args) => (
  <Grid container>
    <Grid {...args} />
    <Grid {...args} />
    <Grid {...args} />
    <Grid {...args} />
  </Grid>
)

// export const Primary = Template.bind({});
// Primary.args = {
//   xs: 0,
// };

// export const Secondary = Template.bind({});
// Secondary.args = {
//   label: 'Grid',
// };

// export const Large = Template.bind({});
// Large.args = {
//   size: 'large',
//   label: 'Grid',
// };

export const Standart = Template.bind({})
Standart.args = {
  fullWidth: true,
  xs: 4,
  style: { border: '1px solid #eee', height: '100px' },
}
