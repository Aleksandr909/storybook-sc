import styled from 'styled-components'

export interface IGridProps {
    xs?: number | boolean;
    sm?: number | boolean;
    md?: number | boolean;
    lg?: number | boolean;
    xl?: number | boolean;
    container?: boolean;
    justify?: 'center' | 'flex-end' | 'flex-start' | 'space-between';
    alignItems?: 'center' | 'flex-end' | 'flex-start';
    nowrap?: boolean;
    fullWidth?: boolean;
    fullHeight?: boolean;
    direction?: 'column' | 'row';
    gap?: number;
}

const getSizeParams = (numOrBool: number | boolean | undefined) => {
  if (!numOrBool) return ''
  if (numOrBool === true) return 'flex: 1;'
  return `flex-basis: ${(100 / 12) * numOrBool}%;`
}

const Grid = styled.div<IGridProps>((props) => `
    box-sizing: border-box;
    width: ${props.fullWidth ? '100%' : 'auto'};
    height: ${props.fullHeight ? '100%' : 'auto'};
    ${props.container ? 'display: flex' : ''};
    ${props.container && props.gap ? `
        margin: -${props.gap * 8}px; 
        width: calc(100% + ${props.gap * 16}px); 
        & > * {margin: ${props.gap * 8}px};
    ` : ''}
    flex-wrap: ${props.nowrap ? 'nowrap' : 'wrap'};
    ${getSizeParams(props.xs)}
    ${props.justify ? `justify-content:${props.justify};` : ''}
    ${props.alignItems ? `align-items:${props.alignItems};` : ''}
    ${props.direction ? `flex-direction:${props.direction};` : ''}
    box-sizing: border-box;
    @media(min-width: 600px) {
        ${getSizeParams(props.sm)}
    };
    @media(min-width: 960px) {
        ${getSizeParams(props.md)}
    };
    @media(min-width: 1280px) {
        ${getSizeParams(props.lg)}
    };
    @media(min-width: 1920px) {
        ${getSizeParams(props.xl)}
    }
`)

export default Grid
