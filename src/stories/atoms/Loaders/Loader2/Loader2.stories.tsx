import { ThemeProvider } from 'styled-components'
import React from 'react'
import { Story, Meta } from '@storybook/react/types-6-0'
import themeObj from '../../../../core/theme'
import Loader, { Props } from './Loader'

export default {
  title: 'Atoms/Loaders/Loader2',
  component: Loader,
  parameters: {
    docs: {
      description: {
        component: 'https://codepen.io/kathykato/pen/YzKGrqd',
      },
    },
  },
} as Meta

interface StoryProps extends Props {
  darkMode: boolean;
  children: React.ReactNode;
}

const Template: Story<StoryProps> = (args) => (
  <ThemeProvider theme={{ ...themeObj, style: args.darkMode ? 'dark' : 'light' }}>
    <Loader {...args} />
  </ThemeProvider>
)

export const Primary = Template.bind({})
Primary.args = {
  darkMode: false,
  color: 'primary',
  children: 'button',
}

export const Secondary = Template.bind({})
Secondary.args = {
  darkMode: false,
  color: 'secondary',
  children: 'button',
}
