import styled from 'styled-components'
import React from 'react'

export interface Props {
    color?: 'primary' | 'secondary';
}

const LoaderContainer = styled.div<Props>`
  font-family: 'Roboto', sans-serif;
  font-size: 1rem;
  line-height: 1.5;
  position: relative;
  * {
    box-sizing: border-box;
    &::before, &::after {
      box-sizing: border-box;
    }
  }
  .divider {
    position: absolute;
    z-index: 2;
    top: 65px;
    left: 160px;
    width: 20px;
    height: 15px;
    background: #fff;
  }
  .loading-text {
    position: relative;
    font-size: 3.75rem;
    font-weight: 300;
    margin: 0;
    white-space: nowrap;
    &::before {
      position: absolute;
      content: "";
      z-index: 1;
      top: 40px;
      left: 85px;
      width: 6px;
      height: 6px;
      background: #000;
      border-radius: 50%;
      animation: dotMove 1800ms cubic-bezier(0.25,0.25,0.75,0.75) infinite;
    }
    .letter {
      display: inline-block;
      position: relative;
      color: #000;
      letter-spacing: 8px;
      &:nth-child(1) {
        transform-origin: 100% 70%;
        transform: scale(1, 1.275);
        &::before {
          position: absolute;
          content: "";
          top: 22px;
          left: 0;
          width: 14px;
          height: 36px;
          background: #fff;
          transform-origin: 100% 0;
          animation: lineStretch 1800ms cubic-bezier(0.25,0.25,0.75,0.75) infinite;
        }
      }
      &:nth-child(5) {
        transform-origin: 100% 70%;
        animation: letterStretch 1800ms cubic-bezier(0.25,0.23,0.73,0.75) infinite;
        &::before {
          position: absolute;
          content: "";
          top: 15px;
          left: 2px;
          width: 9px;
          height: 15px;
          background: #fff;
        }
      }
    }
  }
  @keyframes dotMove {
    0%, 100% {
      transform: rotate(180deg) translate(-81px, -10px) rotate(-180deg);
    }
    50% {
      transform: rotate(0deg) translate(-81px, 10px) rotate(0deg);
    }
  }
  @keyframes letterStretch {
    0%, 100% {
      transform: scale(1, 0.35);
      transform-origin: 100% 75%;
    }
    8%, 28% {
      transform: scale(1, 1.48);
      transform-origin: 100% 67%;
    }
    37% {
      transform: scale(1, 0.875);
      transform-origin: 100% 75%;
    }
    46% {
      transform: scale(1, 1.03);
      transform-origin: 100% 75%;
    }
    50%, 97% {
      transform: scale(1);
      transform-origin: 100% 75%;
    }
  }
  @keyframes lineStretch {
    0%, 45%, 70%, 100% {
      transform: scaleY(0.125);
    }
    49% {
      transform: scaleY(0.75);
    }
    50% {
      transform: scaleY(0.875);
    }
    53% {
      transform: scaleY(0.5);
    }
    60% {
      transform: scaleY(0);
    }
    68% {
      transform: scaleY(0.18);
    }
  }
`

const Loader = (props: Props) => (
  <LoaderContainer {...props}>
    <div className="divider" aria-hidden="true" />
    <p className="loading-text" aria-label="Loading">
      <span className="letter" aria-hidden="true">L</span>
      <span className="letter" aria-hidden="true">o</span>
      <span className="letter" aria-hidden="true">a</span>
      <span className="letter" aria-hidden="true">d</span>
      <span className="letter" aria-hidden="true">i</span>
      <span className="letter" aria-hidden="true">n</span>
      <span className="letter" aria-hidden="true">g</span>
    </p>
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400&display=swap" rel="stylesheet" />
  </LoaderContainer>
)

export default Loader
