import styled from 'styled-components'
import React, { HTMLAttributes } from 'react'

interface ContainerProps extends HTMLAttributes<HTMLDivElement> {
  color?: 'primary' | 'secondary' | 'default';
}

const LoaderContainer = styled.div<ContainerProps>`
  width: 300px;
  height: 300px;
  display: flex;
  justify-content: center;
  align-items: center;
  & > div {
    width: 150px;
    height: 150px;
    padding: 3px;
    display: flex;
    justify-content: center;
    align-items: center;
    border-radius: 50%;
    background: linear-gradient(0deg, rgba(63,249,220,0.1) 33%, rgba(63,249,220,1) 100%);
    animation: spin .8s linear 0s infinite;
    & > div {
      width: 100%;
      height: 100%;
      background-color: rgba(144,144,144,0.8);
      border-radius: 50%;
    }
  }
  @keyframes spin {
    from {
      transform: rotate(0);
    }
    to{
      transform: rotate(359deg);
    }
  }  
`

export type Props = ContainerProps

const Loader = ({ ...props }: Props) => (
  <LoaderContainer {...props}>
    <div>
      <div />
    </div>
  </LoaderContainer>
)

export default Loader
