import { ThemeProvider } from 'styled-components'
import React from 'react'
import { Story, Meta } from '@storybook/react/types-6-0'
import themeObj from '../../../../core/theme'
import Loader, { Props } from './Loader'

export default {
  title: 'Atoms/Loaders/Loader4',
  component: Loader,
  parameters: {
    docs: {
      description: {
        component: 'https://codepen.io/Curlmuhi/pen/ExKWXKO',
      },
    },
  },
} as Meta

interface StoryProps extends Props {
  darkMode: boolean;
  text: string;
}

const Template: Story<StoryProps> = ({ darkMode, ...args }: StoryProps) => (
  <ThemeProvider theme={{ ...themeObj, style: darkMode ? 'dark' : 'light' }}>
    <Loader {...args} />
  </ThemeProvider>
)

export const index = Template.bind({})
index.args = {
  darkMode: false,
  text: 'Загрузка',
}
