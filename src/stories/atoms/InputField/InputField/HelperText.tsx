import styled from 'styled-components'

interface Props {
}
const HelperText = styled.p<Props>`
    display: block;
    font-size: 12px;
    width: fit-content;
    margin: 2px 8px;
    z-index: 1;
    position: relative;
`

export default HelperText
