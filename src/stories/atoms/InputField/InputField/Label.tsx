import styled from 'styled-components'

interface Props {}
const Label = styled.label<Props>`
    display: block;
    font-size: 14px;
    width: fit-content;
    margin-left: 8px;
    z-index: 1;
    position: relative;
`

export default Label
