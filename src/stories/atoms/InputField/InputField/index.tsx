import * as React from 'react'
import Label from './Label'
import InputContainer from './InputContainer'
import Input from './Input'
import HelperText from './HelperText'

export interface Props extends React.InputHTMLAttributes<HTMLInputElement> {
    label?: string;
    fullWidth?: boolean;
    error?: boolean;
    errorText?: string;
}

const InputField: React.FunctionComponent<Props> = (
  {
    label, id, fullWidth, error, errorText, ...inputProps
  }: Props,
) => {
  const [isFocused, setIsFocused] = React.useState(false)
  const onFocus = (e: React.FocusEvent<HTMLInputElement>) => {
    if (inputProps?.onFocus) inputProps.onFocus(e)
    setIsFocused(true)
  }
  const onBlur = (e: React.FocusEvent<HTMLInputElement>) => {
    if (inputProps?.onBlur) inputProps.onBlur(e)
    setIsFocused(false)
  }

  return (
    <InputContainer fullWidth={fullWidth} error={error} isFocused={isFocused}>
      {label && <Label htmlFor={id}>{label}</Label>}
      <Input id={id} {...inputProps} onFocus={onFocus} onBlur={onBlur} />
      {error && <HelperText>{errorText}</HelperText>}
    </InputContainer>
  )
}

export default InputField
