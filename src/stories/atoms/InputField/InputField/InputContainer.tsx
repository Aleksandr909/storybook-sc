import styled from 'styled-components'

interface IInputContainerProps{
    fullWidth?: boolean;
    error?: boolean;
    isFocused?: boolean;
}

const InputContainer = styled.div<IInputContainerProps>`
    position: relative;
    width: ${({ fullWidth }) => (fullWidth ? '100%' : 'fit-content')};
    margin: 8px 0;
    color: ${({ theme, error, isFocused }) => ((error && theme.colors.secondary[theme.style])
        || (isFocused && theme.colors.primary[theme.style])
        || 'inherit')};
    & > input {
        border-color: ${({ theme, error, isFocused }) => ((error && theme.colors.secondary[theme.style])
            || (isFocused && theme.colors.primary[theme.style]))} !important;
    }
`

export default InputContainer
