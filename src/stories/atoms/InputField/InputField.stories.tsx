import { ThemeProvider } from 'styled-components'
import React from 'react'
// also exported from '@storybook/react' if you can deal with breaking changes in 6.1
import { Story, Meta } from '@storybook/react/types-6-0'
import themeObj from '../../../core/theme'
import InputField, { Props } from './InputField'

export default {
  title: 'Atoms/InputField',
  component: InputField,
} as Meta

interface StoryProps extends Props {
  darkMode: boolean;
}

const Template: Story<StoryProps> = ({ darkMode, ...args }: StoryProps) => (
  <ThemeProvider theme={{ ...themeObj, style: darkMode ? 'dark' : 'light' }}>
    <InputField {...args} />
  </ThemeProvider>
)

export const index = Template.bind({})
index.args = {
  label: 'InputField',
  darkMode: false,
}
