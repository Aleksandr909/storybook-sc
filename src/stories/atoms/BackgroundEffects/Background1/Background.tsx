/* eslint-disable react/no-this-in-sfc */
import styled from 'styled-components'
import React, { useRef } from 'react'

export type Props = React.HTMLAttributes<HTMLDivElement>

const BackgroundContainer = styled.div<Props>`
  width: 100%;
  height: 400px;
  display: flex;
  align-items: center;
  justify-content: center;
  position: relative;
  overflow: hidden;
  margin: 5px;
  cursor: pointer;
  border-radius: 2px;
  box-shadow: 0 5px 5px rgba(0, 0, 0, 0.02), inset 0 0px 0px 1px rgba(0, 0, 0, 0.07);
  transform: translateZ(0);
  &:hover {
    circle, image {
      transform: scale(1)
    }
  }
  svg {
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
  }
  circle {
    transform-origin: 50% 50%;
    transform: scale(0);
    transition: transform 200ms cubic-bezier(0.250, 0.460, 0.450, 0.940);
  }
  image {
    transform: scale(1.1);
    transform-origin: 50% 50%;
    transition: transform 200ms cubic-bezier(0.250, 0.460, 0.450, 0.940);
  }
`

const Background = (props: Props) => {
  const svgRef = useRef<null | SVGSVGElement>(null) as React.MutableRefObject<SVGSVGElement>
  const itemRef = useRef<null | HTMLDivElement>(null) as React.MutableRefObject<HTMLDivElement>
  const circleRef = useRef<null | SVGCircleElement>(
    null,
  ) as React.MutableRefObject<SVGCircleElement>

  function getCoordinates(e: React.MouseEvent<HTMLDivElement> | React.Touch) {
    const point = svgRef.current.createSVGPoint()
    point.x = e.clientX
    point.y = e.clientY
    return point.matrixTransform(svgRef.current.getScreenCTM()?.inverse())
  }
  const update = (c: DOMPoint) => {
    circleRef.current.setAttribute('cx', c.x.toString())
    circleRef.current.setAttribute('cy', c.y.toString())
  }
  const mouseMoveHandler = (e: React.MouseEvent<HTMLDivElement>) => {
    update(getCoordinates(e))
  }
  const touchMoveHandler = (e: React.TouchEvent<HTMLDivElement>) => {
    e.preventDefault()
    const touch = e.targetTouches[0]
    if (touch) update(getCoordinates(touch))
  }
  return (
    <BackgroundContainer
      {...props}
      ref={itemRef}
      onMouseMove={mouseMoveHandler}
      onTouchMove={touchMoveHandler}
    >
      <svg ref={svgRef} preserveAspectRatio="xMidYMid slice" viewBox="0 0 600 400">
        <defs>
          <clipPath id="clip-0">
            <circle cx="0" cy="0" fill="#000" r="150px" ref={circleRef} />
          </clipPath>
        </defs>
        <g clipPath="url(#clip-0)">
          <image
            height="100%"
            preserveAspectRatio="xMidYMid slice"
            width="100%"
            xlinkHref="https://www.vorfolio.ru/static/media/shorelin_lg.3d912713.png"
          />
        </g>
      </svg>
    </BackgroundContainer>
  )
}

export default Background
