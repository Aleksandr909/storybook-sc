/* eslint-disable jsx-a11y/mouse-events-have-key-events */
import styled from 'styled-components'
import React, { useRef } from 'react'

export type Props = React.HTMLAttributes<HTMLDivElement>

const BackgroundContainer = styled.div<Props>`
  cursor: none;
  #cursorBlob {
    width: 50px;
    height: 50px;
    background: linear-gradient(
      120deg,
      #FF1744,
      #E040FB,
      #2979FF,
      #00E5FF,
      #76FF03
    );
    background-size: 1600% 1600%;
    position: absolute;
    mix-blend-mode: difference;
    pointer-events: none;
    z-index: 1;
    transition: 0.15s linear;
    animation: blobRadius 5s ease infinite, blobBackground 15s ease infinite;
  }
  .wrap {
    display: flex;
    justify-content: center;
    align-items: center;
    height: 100vh;
    width: 100vw;
    background: #212121;
  }
  .nav {
    display: flex;
    flex-direction: column;
    margin: 0;
    padding: 0;
    &__link {
      color: #fff;
      list-style: none;
      cursor: none;
      font-size: 10vw;
      font-family: "Montserrat", sans-serif;
      transition: 0.25s ease;
      &:not(:last-child) {
        margin-bottom: 50px;
      }
    }
  }
  @keyframes blobRadius {
    0%, 100% { border-radius: 43% 77% 80% 40% / 40% 40% 80% 80%; }
    20% { border-radius: 47% 73% 61% 59% / 47% 75% 45% 73%; }
    40% { border-radius: 46% 74% 74% 46% / 74% 58% 62% 46%; }
    60% { border-radius: 47% 73% 61% 59% / 40% 40% 80% 80%; }
    80% { border-radius: 50% 70% 52% 68% / 51% 61% 59% 69%; }
  }
  @keyframes blobBackground {
    0%, 100% { background-position: 0% 50%; }
    50% { background-position: 100% 50%; }
  }
`

const Background = (props: Props) => {
  const cursorRef = useRef<null | HTMLDivElement>(null) as React.MutableRefObject<HTMLDivElement>
  const setCursorHover = () => { cursorRef.current.style.transform = 'scale(2.5)' }
  const removeCursorHover = () => { cursorRef.current.style.transform = '' }
  const setCursorPos = (e: React.MouseEvent<HTMLDivElement>) => {
    const CURSOR = cursorRef.current
    const { pageX: posX, pageY: posY } = e
    CURSOR.style.top = `${posY - (CURSOR.offsetHeight / 2)}px`
    CURSOR.style.left = `${posX - (CURSOR.offsetWidth / 2)}px`
  }
  const mouseMoveHandler = (e: React.MouseEvent<HTMLDivElement>) => {
    setCursorPos(e)
  }
  return (
    <BackgroundContainer
      {...props}
      onMouseMove={mouseMoveHandler}
    >
      <div id="cursorBlob" ref={cursorRef} />
      <div className="wrap">
        <ul className="nav">
          <li
            className="nav__link"
            onMouseOver={setCursorHover}
            onMouseLeave={removeCursorHover}
          >
            About Us
          </li>
          <li
            className="nav__link"
            onMouseOver={setCursorHover}
            onMouseLeave={removeCursorHover}
          >
            Why Us
          </li>
          <li
            className="nav__link"
            onMouseOver={setCursorHover}
            onMouseLeave={removeCursorHover}
          >
            Contact Us
          </li>
        </ul>
      </div>
    </BackgroundContainer>
  )
}

export default Background
