import { ThemeProvider } from 'styled-components'
import React from 'react'
import { Story, Meta } from '@storybook/react/types-6-0'
import themeObj from '../../../../core/theme'
import Background, { Props } from './Background'

export default {
  title: 'Atoms/BackgroundsEffects/Background2',
  component: Background,
  parameters: {
    docs: {
      description: {
        component: 'https://codepen.io/markmead/pen/ErMoKO',
      },
    },
  },
} as Meta

interface StoryProps extends Props {
  darkMode: boolean;
}

const Template: Story<StoryProps> = (args) => (
  <ThemeProvider theme={{ ...themeObj, style: args.darkMode ? 'dark' : 'light' }}>
    <Background {...args} />
  </ThemeProvider>
)

export const index = Template.bind({})
