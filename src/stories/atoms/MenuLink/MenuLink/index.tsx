import styled from 'styled-components'
import Link from './Link'

const MenuLink = styled(Link)`
    position: relative;
    color: ${({ theme }) => theme.colors.textPrimary[theme.style]};
    cursor: pointer;
    &:after {
        content: "";
        height: 2px;
        position: absolute;
        left: 50%;
        bottom: -2px;
        width: 0;
        border-radius: 1px;
        background: ${({ theme }) => theme.colors.linkLine[theme.style]};
        transition: all .5s ease;
    }
    &:hover {
        &:after {
            left: 0;
            width: 100%;
        }
    }
`

export default MenuLink
