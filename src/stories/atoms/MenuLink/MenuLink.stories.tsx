import { ThemeProvider } from 'styled-components'
import React from 'react'
// also exported from '@storybook/react' if you can deal with breaking changes in 6.1
import { Story, Meta } from '@storybook/react/types-6-0'
import themeObj from '../../../core/theme'
import MenuLink from './MenuLink'

export default {
  title: 'Atoms/MenuLink',
  component: MenuLink,
} as Meta

interface StoryProps {
  darkMode?: boolean;
  children?: React.ReactNode;
}

const Template: Story<StoryProps> = ({ darkMode, ...args }) => (
  <ThemeProvider theme={{ ...themeObj, style: darkMode ? 'dark' : 'light' }}>
    <MenuLink {...args} />
  </ThemeProvider>
)

export const index = Template.bind({})
index.args = {
  children: 'LINK',
  darkMode: false,
}
