import styled from 'styled-components'
import React from 'react'

export interface Props extends React.HTMLAttributes<HTMLDivElement>{
  color?: 'primary' | 'secondary' | 'default';
}

const ScrollToDownIco = styled.div<Props>`
  width: 0;
  height: 40px;
  border: ${({ theme, color = 'default' }) => `1px solid ${theme.colors[color][theme.style]}`};
  position: relative;
  animation: scroll 1.5s infinite;
  -webkit-animation: scroll 1.5s infinite;
  &::after {
    content: '';
    display: block;
    position: absolute;
    top: 100%;
    left: -5px;
    width: 1px;
    height: 10px;
    border-top: ${({ theme, color = 'default' }) => `10px solid ${theme.colors[color][theme.style]}`};
    border-left: 5px solid transparent;
    border-right: 5px solid transparent;
  }
  @keyframes scroll {
    0% {
      height: 40px;
    }
    30% {
      height: 70px;
    }
    60% {
      height: 40px;
    }
  }
`

export default ScrollToDownIco
