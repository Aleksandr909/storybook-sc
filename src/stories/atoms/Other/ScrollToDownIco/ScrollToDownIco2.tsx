import styled from 'styled-components'
import React from 'react'

export interface Props extends React.HTMLAttributes<HTMLDivElement>{
  color?: 'primary' | 'secondary' | 'default';
}

const ScrollToDownIco = styled.div<Props>`
  width: 60px;
  height: 60px;
  border: ${({ theme, color = 'default' }) => `2px solid ${theme.colors[color][theme.style]}`};
  border-radius: 50%;
  position: relative;
  animation: down 1.5s infinite;
  -webkit-animation: down 1.5s infinite;
  &::before {
    content: '';
    position: absolute;
    top: 15px;
    left: 18px;
    width: 18px;
    height: 18px;
    border-left: ${({ theme, color = 'default' }) => `2px solid ${theme.colors[color][theme.style]}`};
    border-bottom: ${({ theme, color = 'default' }) => `2px solid ${theme.colors[color][theme.style]}`};
    transform: rotate(-45deg);
  }
  @keyframes down {
    0% {
      transform: translate(0);
    }
    20% {
      transform: translateY(15px);
    }
    40% {
      transform: translate(0);
    }
  }
`

export default ScrollToDownIco
