import React from 'react'
import ScrollToDownIco3 from './ScrollToDownIco3'
import ScrollToDownIco2 from './ScrollToDownIco2'
import ScrollToDownIco1 from './ScrollToDownIco1'

export interface Props extends React.HTMLAttributes<HTMLDivElement>{
    variant?: 'mobile' | 'arrow' | 'arrow-circle';
    color?: 'primary' | 'secondary' | 'default';
}
const variants = {
  mobile: ScrollToDownIco1,
  arrow: ScrollToDownIco2,
  'arrow-circle': ScrollToDownIco3,
}

const ScrollToDownIco = ({ variant, ...props }: Props) => {
  const Component = variants[variant || 'arrow']
  return (
    <Component {...props} />
  )
}

export default ScrollToDownIco
