import styled from 'styled-components'
import React from 'react'

export interface Props extends React.HTMLAttributes<HTMLDivElement>{
  color?: 'primary' | 'secondary' | 'default';
}

const ScrollToDownIco = styled.div<Props>`
  width: 50px;
  height: 90px;
  border: ${({ theme, color = 'default' }) => `3px solid ${theme.colors[color][theme.style]}`};
  border-radius: 60px;
  position: relative;
  &::before {
    content: '';
    width: 12px;
    height: 12px;
    position: absolute;
    top: 10px;
    left: 50%;
    transform: translateX(-50%);
    background-color: ${({ theme, color = 'default' }) => theme.colors[color][theme.style]};
    border-radius: 50%;
    opacity: 1;
    animation: wheel 2s infinite;
    -webkit-animation: wheel 2s infinite;
  }
  @keyframes wheel {
    to {
      opacity: 0;
      top: 60px;
    }
  }
`

export default ScrollToDownIco
