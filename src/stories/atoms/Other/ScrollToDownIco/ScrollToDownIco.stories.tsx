import { ThemeProvider } from 'styled-components'
import React from 'react'
import { Story, Meta } from '@storybook/react/types-6-0'
import themeObj from '../../../../core/theme'
import ScrollToDownIco3 from './ScrollToDownIco3'
import ScrollToDownIco2 from './ScrollToDownIco2'
import ScrollToDownIco1, { Props } from './ScrollToDownIco1'
import ScrollToDownIco, { Props as TogetherProps } from './ScrollToDownIco'

export default {
  title: 'Atoms/Other/ScrollToDownIco',
  component: ScrollToDownIco,
  parameters: {
    docs: {
      description: {
        component: 'https://codepen.io/TKS31/pen/gOaKaxx',
      },
    },
  },
} as Meta

interface StoryProps extends Props {
  darkMode: boolean;
}

const Singly: Story<StoryProps> = (args) => (
  <ThemeProvider theme={{ ...themeObj, style: args.darkMode ? 'dark' : 'light' }}>
    <ScrollToDownIco1 {...args} />
    <ScrollToDownIco2 {...args} />
    <ScrollToDownIco3 {...args} />
  </ThemeProvider>
)
interface StoryProps extends TogetherProps {
  darkMode: boolean;
}
const Together: Story<StoryProps> = (args) => (
  <ThemeProvider theme={{ ...themeObj, style: args.darkMode ? 'dark' : 'light' }}>
    <ScrollToDownIco {...args} />
  </ThemeProvider>
)

export const singly = Singly.bind({})
singly.args = {
  darkMode: false,
}

export const together = Together.bind({})
together.args = {
  variant: 'mobile',
}
