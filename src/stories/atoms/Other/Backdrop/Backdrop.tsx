import styled from 'styled-components'
import React from 'react'

export interface Props extends React.HTMLAttributes<HTMLDivElement>{
  isOpen?: boolean;
  invisible?: boolean;
}

const Backdrop = styled.div<Props>`
  width: 100vw;
  height: 100vh;
  position: fixed;
  top:0;
  left: 0;
  z-index: 10;
  background: ${({ invisible }) => (!invisible && 'rgba(0,0,0,0.5)')};
  display: ${({ isOpen }) => (isOpen ? 'block' : 'none')}
`

export default Backdrop
