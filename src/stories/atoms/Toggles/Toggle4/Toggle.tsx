import styled from 'styled-components'
import React, { HTMLAttributes } from 'react'

export interface ContainerProps extends HTMLAttributes<HTMLDivElement> {
  color?: 'primary' | 'secondary' | 'default';
}

const ToggleContainer = styled.div<ContainerProps>`
  position: absolute;
  top: calc(50% - 10px);
  left: calc(50% - 20px);
  & input { display: none}
  .toggle{
    position: relative;
    display: block;
    width: 40px;
    height: 20px;
    cursor: pointer;
    -webkit-tap-highlight-color: transparent;
    transform: translate3d(0,0,0);
    &:before {
      content: "";
      position: relative;
      top: 3px;
      left: 3px;
      width: 34px;
      height: 14px;
      display: block;
      background: #9A9999;
      border-radius: 8px;
      transition: background .2s ease;
    }
    span {
      position: absolute;
      top: 0;
      left: 0;
      width: 20px;
      height: 20px;
      display: block;
      background: white;
      border-radius: 10px;
      box-shadow: 0 3px 8px #9A9999;
      transition: all .2s ease;
      &:before{
        content: "";
        position: absolute;
        display: block;
        margin: -18px;
        width: 56px;
        height: 56px;
        background: ${({ theme, color = 'default' }) => theme.colors[color][theme.style]};
        opacity: 0.5;
        border-radius: 50%;
        transform: scale(0);
        opacity: 1;
        pointer-events: none;
      }
    }
  }
  #cbx:checked + .toggle {
    &:before {
      background: ${({ theme, color = 'default' }) => theme.colors[color][theme.style]};
      opacity: 0.5;
    }
    span {
      background: ${({ theme, color = 'default' }) => theme.colors[color][theme.style]};
      transform: translateX(20px);
      transition: all .2s cubic-bezier(.8,.4,.3,1.25), background .15s ease;
      box-shadow: ${({ theme, color = 'default' }) => `1px 1px 4px ${theme.colors[color][theme.style]}`};
      &:before {
        transform: scale(1);
        opacity: 0;
        transition: all .4s ease;
      }
    }
  }
`

export interface Props extends HTMLAttributes<HTMLInputElement> {
  color?: 'primary' | 'secondary' | 'default';
  label: string;
  id: string;
}
const Toggle = ({
  color, label, id, ...props
}: Props) => {
  const idc = id || `${+new Date()}_${Math.random()}`
  return (
    <ToggleContainer color={color}>
      <input type="checkbox" id={idc} {...props} />
      <label htmlFor={idc} className="toggle">
        <span />
      </label>
    </ToggleContainer>
  )
}

export default Toggle
