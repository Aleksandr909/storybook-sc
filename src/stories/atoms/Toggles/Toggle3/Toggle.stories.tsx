import { ThemeProvider } from 'styled-components'
import React from 'react'
// also exported from '@storybook/react' if you can deal with breaking changes in 6.1
import { Story, Meta } from '@storybook/react/types-6-0'
import themeObj from '../../../../core/theme'
import Toggle, { Props } from './Toggle'

export default {
  title: 'Atoms/Toggle/Toggle3-Checkbox',
  component: Toggle,
  parameters: {
    docs: {
      description: {
        component: 'https://codepen.io/avstorm/pen/deRvMy',
      },
    },
  },
} as Meta

interface StoryProps extends Props {
  darkMode: boolean;
}
const Template: Story<StoryProps> = ({ darkMode, ...args }: StoryProps) => (
  <ThemeProvider theme={{ ...themeObj, style: darkMode ? 'dark' : 'light' }}>
    <Toggle {...args} />
  </ThemeProvider>
)

export const index = Template.bind({})
index.args = {
  darkMode: false,
  label: 'label',
  id: 'id',
}
