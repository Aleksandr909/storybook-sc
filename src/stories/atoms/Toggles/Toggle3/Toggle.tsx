import styled from 'styled-components'
import React, { HTMLAttributes } from 'react'

export interface ContainerProps extends HTMLAttributes<HTMLDivElement> {
  color?: 'primary' | 'secondary' | 'default';
}

const ToggleContainer = styled.div<ContainerProps>`
  width: fit-content;
  display: flex;
  align-items: center;
  & > label {
    cursor: pointer;
  }
  & > .check {
    cursor: pointer;
    position: relative;
    margin: auto;
    width: 18px;
    height: 18px;
    -webkit-tap-highlight-color: transparent;
    transform: translate3d(0, 0, 0);
    width: fit-content;
    margin-right: 10px;
    &:before {
      content: "";
      position: absolute;
      top: -15px;
      left: -15px;
      width: 48px;
      height: 48px;
      border-radius: 50%;
      background: rgba(34,50,84,0.03);
      opacity: 0;
      transition: opacity 0.2s ease;
    }
    & > svg {
      position: relative;
      z-index: 1;
      fill: none;
      stroke-linecap: round;
      stroke-linejoin: round;
      stroke: #C8CCD4;
      stroke-width: 1.5;
      transform: translate3d(0, 0, 0);
      transition: all 0.2s ease;
      & > path {
        stroke-dasharray: 60;
        stroke-dashoffset: 0;
      }
      & > polyline {
        stroke-dasharray: 22;
        stroke-dashoffset: 66;
      }
    }
  }
  &:hover:before {
    opacity: 1;
  }
  &:hover svg {
    stroke: ${({ theme, color = 'default' }) => theme.colors[color][theme.style]};
  }
  & > input{
    display: none;
    &:checked + .check svg {
      stroke: ${({ theme, color = 'default' }) => theme.colors[color][theme.style]};
      & > path {
        stroke-dashoffset: 60;
        transition: all 0.3s linear;
      }
      & > polyline {
        stroke-dashoffset: 42;
        transition: all 0.2s linear;
        transition-delay: 0.15s;
      }
    }
  }
`

export interface Props extends HTMLAttributes<HTMLInputElement> {
  color?: 'primary' | 'secondary' | 'default';
  label: string;
}
const Toggle = ({
  color, label, id, ...props
}: Props) => (
  <ToggleContainer color={color} {...props}>
    <input type="checkbox" id={id} />
    <label htmlFor={id} className="check">
      <svg width="18px" height="18px" viewBox="0 0 18 18">
        <path d="M1,9 L1,3.5 C1,2 2,1 3.5,1 L14.5,1 C16,1 17,2 17,3.5 L17,14.5 C17,16 16,17 14.5,17 L3.5,17 C2,17 1,16 1,14.5 L1,9 Z" />
        <polyline points="1 9 7 14 15 4" />
      </svg>
    </label>
    <label htmlFor={id}>
      {label}
    </label>
  </ToggleContainer>
)

export default Toggle
