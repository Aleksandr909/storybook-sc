import styled from 'styled-components'
import React, { HTMLAttributes } from 'react'

export interface ContainerProps extends HTMLAttributes<HTMLDivElement> {
  color?: 'primary' | 'secondary' | 'default';
}

const ToggleContainer = styled.div<ContainerProps>`
  box-sizing: border-box;
  & > label {
    display: inline-block;
    user-select: none;
    cursor: pointer;
    padding: 6px 8px;
    border-radius: 6px;
    overflow: hidden;
    transition: all 0.2s ease;
    color: ${({ theme }) => theme.colors.textPrimary[theme.style]};
    &:hover {
      background: rgba(144,144,144,0.05);
      & span:first-child {
        border-color: ${({ theme, color = 'default' }) => theme.colors[color][theme.style]};
      }
    }
    & span {
      display: flex;
      align-items: center;
      justify-content: center;
      float: left;
      vertical-align: middle;
      transform: translate3d(0, 0, 0);
      &:first-child {
        position: relative;
        width: 18px;
        height: 18px;
        border-radius: 4px;
        transform: scale(1);
        border: 1px solid #cccfdb;
        transition: all 0.2s ease;
        box-shadow: 0 1px 1px rgba(0,16,75,0.05);
      }
      &:first-child svg {
        position: absolute;
        fill: none;
        stroke: #fff;
        stroke-width: 2;
        stroke-linecap: round;
        stroke-linejoin: round;
        stroke-dasharray: 16px;
        stroke-dashoffset: 16px;
        transition: all 0.3s ease;
        transition-delay: 0.1s;
        transform: translate3d(0, 0, 0);
      }
      &:last-child {
        padding-left: 8px;
        line-height: 18px;
      }
    }
  }
  & > input {
    position: absolute;
    visibility: hidden;
  }
  & > input:checked + label span:first-child {
    background: ${({ theme, color = 'default' }) => theme.colors[color][theme.style]};
    border-color: ${({ theme, color = 'default' }) => theme.colors[color][theme.style]};
    animation: wave 0.4s ease;
  }
  & > input:checked + label span:first-child svg {
    stroke-dashoffset: 0;
    stroke: ${({ theme }) => theme.colors.antiTextPrimary[theme.style]};
  }
  & > svg {
    position: absolute;
    width: 0;
    height: 0;
    pointer-events: none;
    user-select: none;
  }
  @keyframes wave {
    50% {
      transform: scale(0.9);
    }
  }
`

export interface Props extends HTMLAttributes<HTMLInputElement> {
  color?: 'primary' | 'secondary' | 'default';
  label: string;
}
const Toggle = ({ color, label, ...props }: Props) => (
  <ToggleContainer color={color}>
    <input id="morning" type="checkbox" {...props} />
    <label htmlFor="morning">
      <span>
        <svg width="12px" height="10px">
          <use xlinkHref="#check" />
        </svg>
      </span>
      <span>{label}</span>
    </label>
    <svg>
      <symbol id="check" viewBox="0 0 12 10">
        <polyline points="1.5 6 4.5 9 10.5 1" />
      </symbol>
    </svg>
  </ToggleContainer>
)

export default Toggle
