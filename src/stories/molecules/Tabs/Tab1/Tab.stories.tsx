import { ThemeProvider } from 'styled-components'
import React from 'react'
import { Story, Meta } from '@storybook/react/types-6-0'
import themeObj from '../../../../core/theme'
import Tab, { Props } from './Tab'

export default {
  title: 'InWork/Tabs/Tab1',
  component: Tab,
  parameters: {
    docs: {
      description: {
        component: 'https://codepen.io/ainalem/pen/JxEqzW',
      },
    },
  },
  argTypes: {
    color: {
      control: {
        type: 'select',
        options: ['primary', 'secondary', 'default'],
      },
    },
  },
} as Meta

interface StoryProps extends Props {
  darkMode: boolean;
  children: React.ReactNode;
}

const Template: Story<StoryProps> = ({ darkMode, ...args }) => (
  <ThemeProvider theme={{ ...themeObj, style: darkMode ? 'dark' : 'light' }}>
    <Tab {...args} />
  </ThemeProvider>
)

export const index = Template.bind({})
index.args = {
  darkMode: false,
  children: 'button',
}
