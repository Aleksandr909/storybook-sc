import { ThemeProvider } from 'styled-components'
import React from 'react'
// also exported from '@storybook/react' if you can deal with breaking changes in 6.1
import { Story, Meta } from '@storybook/react/types-6-0'
import themeObj from '../../../../core/theme'
import Navigation, { Props } from './Navigation'

export default {
  title: 'Molecules/Navigation/Navigation1',
  component: Navigation,
  parameters: {
    docs: {
      description: {
        component: 'https://codepen.io/tobiasglaus/pen/qQLjYZ',
      },
    },
  },
} as Meta

interface StoryProps extends Props {
  darkMode: boolean;
}
const Template: Story<StoryProps> = ({ darkMode, ...args }) => (
  <ThemeProvider theme={{ ...themeObj, style: darkMode ? 'dark' : 'light' }}>
    <Navigation {...args} />
  </ThemeProvider>
)

export const index = Template.bind({})
index.args = {
  darkMode: false,
  label: 'label',
  id: 'id',
}
