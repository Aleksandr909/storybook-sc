/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
import styled from 'styled-components'
import React, {
  HTMLAttributes, useRef, useState,
} from 'react'
import { ReactComponent as Settings } from '../settings.svg'
import { ReactComponent as List } from '../list.svg'
import { ReactComponent as Home } from '../home.svg'
import { ReactComponent as Heart } from '../heart.svg'
import { ReactComponent as Bar } from '../bar.svg'

export interface ContainerProps extends HTMLAttributes<HTMLDivElement> {
  color?: 'primary' | 'secondary' | 'default';
}

const NavigationContainer = styled.div<ContainerProps>`
width:440px;
*{
  margin:0;
  padding:0;
  box-sizing:border-box;
}
--black: #2f3542;
--grey: #a4b0be;
.wave-wrap{
  position:relative;
  width:100%;
  height:22px;
  overflow:hidden;
  margin-bottom:-0.5px;
  & svg {
    position:absolute;
    width:114px;
    bottom:0;
    transform-origin:bottom;
    transform:scaleY(0.8);
    transition:all .6s cubic-bezier(0.23, 1, 0.32, 1);
    .path{
      fill:var(--black);
    }
  }
}
.list-wrap{
  display:flex;
  width:100%;
  height:80px;
  background:var(--black);
  list-style:none;
  justify-content:space-around;
  padding: 0 20px;
  li{
    cursor:pointer;
    position:relative;
    width:100%;
    height:100%;
    display: flex;
    justify-content: center;
    align-items: center;
    transition:all .6s cubic-bezier(0.23, 1, 0.32, 1);
    svg{
      position:relative;
      font-size:1.5em;
      color:var(--grey);
      z-index:5;
      transition:all .6s cubic-bezier(0.23, 1, 0.32, 1);
    }
    &:before{
      content:"";
      position:absolute;
      background:green;
      height:80%;
      width:80%;
      left:10%;
      top:10%;
      border-radius:50%;
      z-index:0;
      transform:scale(0);
      transition:all .6s cubic-bezier(0.23, 1, 0.32, 1);
    }
    &.active{
      margin-top:-10px;
      
      i{
        color:var(--black);
      }
      
      &:before{
        transform:scale(1);
      }
    }
    &:nth-child(1){&:before{background:#537895}}
    &:nth-child(2){&:before{background:#ff6b81}}
    &:nth-child(3){&:before{background:#7bed9f}}
    &:nth-child(4){&:before{background:#70a1ff}}
    &:nth-child(5){&:before{background:#dfe4ea}}
  }
}
`

export interface Props extends HTMLAttributes<HTMLInputElement> {
  color?: 'primary' | 'secondary' | 'default';
  label: string;
  id: string;
}
const Navigation = ({ color, label, ...props }: Props) => {
  const [current, setCurrent] = useState(0)
  const listWrap = useRef<HTMLUListElement | null>(null) as React.MutableRefObject<HTMLUListElement>
  const onClickHandler = (e: React.MouseEvent<HTMLLIElement>, index: number) => {
    setCurrent(index)
  }
  return (
    <NavigationContainer color={color} {...props}>
      <div className="wave-wrap">
        <svg version="1.1" style={{ left: `${current * 80}px` }} xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink" viewBox="0 0 114 26">
          <path
            className="path"
            d="M120.8,26C98.1,26,86.4,0,60.4,0C35.9,0,21.1,26,0.5,26H120.8z"
          />
        </svg>
      </div>
      <ul className="list-wrap" ref={listWrap}>
        <li
          color="linear-gradient(to top, #09203f 0%, #537895 100%)"
          title="Home"
          onClick={(e) => onClickHandler(e, 0)}
          className={current === 0 ? 'active' : ''}
        >
          <Bar />
        </li>
        <li
          color="#ff6b81"
          title="Profile"
          onClick={(e) => onClickHandler(e, 1)}
          className={current === 1 ? 'active' : ''}
        >
          <Heart />
        </li>
        <li
          color="#7bed9f"
          title="Get a beer!"
          onClick={(e) => onClickHandler(e, 2)}
          className={current === 2 ? 'active' : ''}
        >
          <Home />
        </li>
        <li
          color="#70a1ff"
          title="Files"
          onClick={(e) => onClickHandler(e, 3)}
          className={current === 3 ? 'active' : ''}
        >
          <List />
        </li>
        <li
          color="#dfe4ea"
          title="Settings"
          onClick={(e) => onClickHandler(e, 4)}
          className={current === 4 ? 'active' : ''}
        >
          <Settings />
        </li>
      </ul>
    </NavigationContainer>
  )
}

export default Navigation
