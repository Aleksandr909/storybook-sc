import * as React from 'react'
import Grid from '../../atoms/Murkup/Grid/Grid'
import InputField from '../../atoms/InputField/InputField'
import Button from '../../atoms/Buttons/Button7/Button'

interface IFormProps {
}

const Form: React.FunctionComponent<IFormProps> = (props) => (
  <Grid container>
    <Grid xs={12} sm={10} md={6} lg={4} xl={3}>
      {/* <FormContainer> */}
      <InputField
        fullWidth
        label="Email"
        placeholder="Введите email"
        id="email"
        type="text"
        name="email"
      />
      <InputField
        fullWidth
        label="Пароль"
        placeholder="Введите пароль"
        id="password"
        type="password"
        name="password"
      />
      <Grid container fullWidth>
        <Button
          style={{ marginRight: 10 }}
          variant="contained"
        >
          Войти
        </Button>
        <Button
          variant="outlined"
        >
          Регистрация
        </Button>
      </Grid>
      {/* </FormContainer> */}
    </Grid>
  </Grid>
)

export default Form
