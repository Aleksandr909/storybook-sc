import { ThemeProvider } from 'styled-components'
import React from 'react'
// also exported from '@storybook/react' if you can deal with breaking changes in 6.1
import { Story, Meta } from '@storybook/react/types-6-0'
import themeObj from '../../../core/theme'
import Form from './Form'

export default {
  title: 'Molecules/Form',
  component: Form,
} as Meta

interface StoryProps {
  darkMode?: boolean;
  error?: boolean;
}

const Template: Story<StoryProps> = ({ darkMode, ...args }) => (
  <ThemeProvider theme={{ ...themeObj, style: darkMode ? 'dark' : 'light' }}>
    <Form {...args} />
  </ThemeProvider>
)

export const index = Template.bind({})
index.args = {
  darkMode: false,
}
