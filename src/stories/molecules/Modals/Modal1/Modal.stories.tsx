import { ThemeProvider } from 'styled-components'
import React from 'react'
// also exported from '@storybook/react' if you can deal with breaking changes in 6.1
import { Story, Meta } from '@storybook/react/types-6-0'
import themeObj from '../../../../core/theme'
import Modal, { Props } from './Modal'

export default {
  title: 'Molecules/Modal/Modal1',
  component: Modal,
  parameters: {
    docs: {
      description: {
        component: 'https://codepen.io/designcouch/pen/obvKxm',
      },
    },
  },
} as Meta

interface StoryProps extends Props {
  darkMode: boolean;
}
const Template: Story<StoryProps> = ({ darkMode, ...args }) => (
  <ThemeProvider theme={{ ...themeObj, style: darkMode ? 'dark' : 'light' }}>
    <Modal {...args} />
  </ThemeProvider>
)

export const index = Template.bind({})
index.args = {
  darkMode: false,
}
